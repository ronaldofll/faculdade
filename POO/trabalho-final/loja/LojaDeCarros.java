package loja;

import loja.sistema.Sistema;
import loja.produtos.veiculos.tipos.Carro;
import loja.produtos.veiculos.tipos.Moto;
import loja.produtos.veiculos.tipos.Onibus;
import loja.produtos.veiculos.Veiculo;
import loja.produtos.ItemDeVenda;

import java.util.Scanner;
import java.util.InputMismatchException;

/**
* Classe principal do projeto Loja, esta classe inicializará o programa e utilizará a classe Sistema para gerenciar
* os produtos e vendas da loja.
*/
public class LojaDeCarros {

	/**
	* Método principal, que irá executar o programa.
   * @param args Não utilizado.
	*/
	public static void main(String[] args) {

		Sistema sistema = new Sistema();

		/*Carro carro1 = new Carro(2017, "AAA1234", "FIAT", "UNO", 0, 1000, 77, 6, 5, 4);
		Carro carro2 = new Carro(2018, "BBB1234", "FIAT", "UNO", 10000, 1300, 77, 6, 5, 4);
		Moto moto1 = new Moto(2017, "CCC1234", "YAMAHA", "FAZER 250", 0, 250, 100, 6);
		Onibus onibus1 = new Onibus(2008, "DDD1234", "MERCEDES BENZ", "COMIL SVELTO U", 0, 4500, 225, 6, 48, 4);
		
		sistema.cadastrarVeiculo(carro1, 40000.0);
		sistema.cadastrarVeiculo(carro2, 45000.0);
		sistema.cadastrarVeiculo(moto1, 15000.0);
		sistema.cadastrarVeiculo(onibus1, 70000.0);

		sistema.venderVeiculo("AAA1234");

		sistema.listarVeiculosNaoVendidos();*/
		//sistema.listarVeiculosPorAno(2000,2018);
		//sistema.listarVeiculosPorQuilometragem(10000);
		//sistema.listarVeiculosPorMarca("YAMAHA");
		//sistema.listarVeiculosPorModelo("COMIL SVELTO U");

		menu(sistema);
	}

	/**
	* Menu principal, através desse menu é possível acessar todas as ações disponíveis no programa.
	* @param sistema utiliza uma instância de {@link loja.sistema.Sistema Sistema} para executará as ações.
	*/
	private static void menu(Sistema sistema){
		System.out.println("\n..::MENU::..");
		System.out.println("1 - Cadastrar veículo");
		System.out.println("2 - Procurar veículo");
		System.out.println("3 - Vender");
		System.out.println("4 - Caixa");
		System.out.println("0 - Sair\n");

		Scanner s = new Scanner(System.in);
		int opc = 0;
		try{
			System.out.print("Aguardando opção: ");
			opc = s.nextInt();
		}catch(InputMismatchException e){
			System.out.println("Opção inválida, tente novamente!\n");
			menu(sistema);
		}

		switch(opc) {
			case 0: 
				return;
			case 1:
				menuCadastroVeiculo(sistema);
				break;
			case 2:
				menuProcurarVeiculo(sistema);
				break;
			case 3:
				menuVenderVeiculo(sistema);
				break;
			case 4:
				sistema.lerValorDeVendas();
				menu(sistema);
				break;
			default: 
				System.out.println("Opção inválida, tente novamente!\n");
				menu(sistema);
				break;
		}
	}

	/**
	* Menu de cadastro de veículo, neste menu o usuário selecionará qual o tipo de veículo será cadastrado.
	* @param sistema utiliza uma instância de {@link loja.sistema.Sistema Sistema} para executará as ações.
	*/
	private static void menuCadastroVeiculo(Sistema sistema){		
		System.out.println("\n..::TIPO DE VEICULO::..");
		System.out.println("1 - MOTO");
		System.out.println("2 - CARRO");
		System.out.println("3 - ONIBUS");
		System.out.println("0 - Voltar\n");

		Scanner s = new Scanner(System.in);
		int opc = 0;
		try{
			System.out.print("Aguardando opção: ");
			opc = s.nextInt();
		}catch(InputMismatchException e){
			System.out.println("Opção inválida, tente novamente!\n");
			menu(sistema);
		}

		boolean cadastroOk = false;

		try{
			switch(opc) {
				case 0: 
					menu(sistema);
					break;
				case 1:
					cadastroOk = menuCadastroMoto(sistema);
					break;
				case 2:
					cadastroOk = menuCadastroCarro(sistema);
					break;
				case 3:
					cadastroOk = menuCadastroOnibus(sistema);
					break;
				default: 
					System.out.println("Opção inválida, tente novamente!\n");
					menuCadastroVeiculo(sistema);
					break;
			}

		}catch(InputMismatchException e){
			System.out.println("Dados inválidos, tente novamente!\n");
			menuCadastroVeiculo(sistema);
		}

		if(cadastroOk) {
			System.out.println("\nCADASTRO EFETUADO COM SUCESSO!");
		}else {
			System.out.println("\nCADASTRO NÃO EFETUADO!");
		}
		menu(sistema);
	}

	/**
	* Menu de cadastro de moto, neste menu o usuário informará os dados da moto a ser cadastrada.
	* @param sistema utiliza uma instância de {@link loja.sistema.Sistema Sistema} para executará as ações.
	*/
	private static boolean menuCadastroMoto(Sistema sistema) throws InputMismatchException{

		System.out.println("\nDIGITE OS DADOS DA MOTO");

		Scanner s = new Scanner(System.in);

		System.out.print("Ano...............: ");
		int anoFabricacao = s.nextInt();
		System.out.print("Placa.............: ");
		String placa = s.next();
		System.out.print("Marca.............: ");
		String marca = s.next();
		System.out.print("Modelo............: ");
		String modelo = s.next();
		System.out.print("Quilometragem.....: ");
		double quilometragem = s.nextDouble();
		System.out.print("Cilindradas.......: ");
		int cilindradas = s.nextInt();
		System.out.print("Potencia..........: ");
		int potenciaCV = s.nextInt();
		System.out.print("Marchas...........: ");
		int numMarchas = s.nextInt();
		Moto moto = new Moto(anoFabricacao, placa, marca, modelo, quilometragem, cilindradas, potenciaCV, numMarchas);

		System.out.print("Valor.............: ");
		double valor = s.nextDouble();

		return sistema.cadastrarVeiculo(moto, valor);
	}

	/**
	* Menu de cadastro de carro, neste menu o usuário informará os dados do carro a ser cadastrada.
	* @param sistema utiliza uma instância de {@link loja.sistema.Sistema Sistema} para executará as ações.
	*/
	private static boolean menuCadastroCarro(Sistema sistema) throws InputMismatchException{

		System.out.println("\nDIGITE OS DADOS DO CARRO");

		Scanner s = new Scanner(System.in);

		System.out.print("Ano...............: ");
		int anoFabricacao = s.nextInt();
		System.out.print("Placa.............: ");
		String placa = s.next();
		System.out.print("Marca.............: ");
		String marca = s.next();
		System.out.print("Modelo............: ");
		String modelo = s.next();
		System.out.print("Quilometragem.....: ");
		double quilometragem = s.nextDouble();
		System.out.print("Cilindradas.......: ");
		int cilindradas = s.nextInt();
		System.out.print("Potencia..........: ");
		int potenciaCV = s.nextInt();
		System.out.print("Marchas...........: ");
		int numMarchas = s.nextInt();
		System.out.print("Assentos..........: ");
		int numAssentos = s.nextInt();
		System.out.print("Portas............: ");
		int numPortas = s.nextInt();
		Carro carro = new Carro(anoFabricacao, placa, marca, modelo, quilometragem, cilindradas, potenciaCV, numMarchas, numAssentos, numPortas);

		System.out.print("Valor.............: ");
		double valor = s.nextDouble();

		return sistema.cadastrarVeiculo(carro, valor);
	}

	/**
	* Menu de cadastro de onibus, neste menu o usuário informará os dados do onibus a ser cadastrada.
	* @param sistema utiliza uma instância de {@link loja.sistema.Sistema Sistema} para executará as ações.
	*/
	private static boolean menuCadastroOnibus(Sistema sistema) throws InputMismatchException{

		System.out.println("\nDIGITE OS DADOS DO ONIBUS");

		Scanner s = new Scanner(System.in);

		System.out.print("Ano...............: ");
		int anoFabricacao = s.nextInt();
		System.out.print("Placa.............: ");
		String placa = s.next();
		System.out.print("Marca.............: ");
		String marca = s.next();
		System.out.print("Modelo............: ");
		String modelo = s.next();
		System.out.print("Quilometragem.....: ");
		double quilometragem = s.nextDouble();
		System.out.print("Cilindradas.......: ");
		int cilindradas = s.nextInt();
		System.out.print("Potencia..........: ");
		int potenciaCV = s.nextInt();
		System.out.print("Marchas...........: ");
		int numMarchas = s.nextInt();
		System.out.print("Assentos..........: ");
		int numAssentos = s.nextInt();
		System.out.print("Portas............: ");
		int numPortas = s.nextInt();
		Onibus onibus = new Onibus(anoFabricacao, placa, marca, modelo, quilometragem, cilindradas, potenciaCV, numMarchas, numAssentos, numPortas);

		System.out.print("Valor.............: ");
		double valor = s.nextDouble();

		return sistema.cadastrarVeiculo(onibus, valor);
	}

	/**
	* Menu de procura, neste menu o usuário poderá consultar os veículos cadastrados no sistema.
	* @param sistema utiliza uma instância de {@link loja.sistema.Sistema Sistema} para executará as ações.
	*/
	private static void menuProcurarVeiculo(Sistema sistema){		
		System.out.println("\n..::PROCURAR VEÍCULO::..");
		System.out.println("1 - LISTAR NÃO VENDIDOS");
		System.out.println("2 - POR ANO");
		System.out.println("3 - POR MODELO");
		System.out.println("4 - POR MARCA");
		System.out.println("5 - POR QUILOMETRAGEM");
		System.out.println("0 - Voltar\n");

		Scanner s = new Scanner(System.in);
		int opc = 0;
		try{
			System.out.print("Aguardando opção: ");
			opc = s.nextInt();
		}catch(InputMismatchException e){
			System.out.println("Opção inválida, tente novamente!\n");
			menu(sistema);
		}

		try{
			switch(opc) {
				case 0: 
					menu(sistema);
					break;
				case 1:
					sistema.listarVeiculosNaoVendidos();
					break;
				case 2:
					System.out.print("Ano inicial........:");
					int anoInicial = s.nextInt();
					System.out.print("Ano final..........:");
					int anoFinal = s.nextInt();

					sistema.listarVeiculosPorAno(anoInicial, anoFinal);
					break;
				case 3:
					System.out.print("Modelo:");
					String modelo = s.next();

					sistema.listarVeiculosPorModelo(modelo);
					break;
				case 4:
					System.out.print("Marca:");
					String marca = s.next();

					sistema.listarVeiculosPorMarca(marca);
					break;
				case 5:
					System.out.print("Quilometragem inicial........:");
					int quilometragemInicial = s.nextInt();
					System.out.print("Quilometragem final..........:");
					int quilometragemFinal = s.nextInt();

					sistema.listarVeiculosPorQuilometragem(quilometragemInicial, quilometragemFinal);
					break;
				default: 
					System.out.println("Opção inválida, tente novamente!\n");
					menuProcurarVeiculo(sistema);
					break;
			}

		}catch(InputMismatchException e){
			System.out.println("Dados inválidos, tente novamente!\n");
			menuCadastroVeiculo(sistema);
		}
		
		menu(sistema);
	}

	/**
	* Menu de venda, neste menu o usuário informará a placa para venda do veículo.
	* @param sistema utiliza uma instância de {@link loja.sistema.Sistema Sistema} para executará as ações.
	*/
	private static void menuVenderVeiculo(Sistema sistema){	
		System.out.println("\n..::VENDA DE VEÍCULO::..");

		Scanner s = new Scanner(System.in);

		System.out.print("Placa: ");
		String placa = s.next();

		boolean vendido = sistema.venderVeiculo(placa);
		if(vendido) {
			System.out.println("\nVEICULO VENDIDO COM SUCESSO!\n");
		}else{
			System.out.println("\nVEÌCULO NÂO ENCONTRADO PARA VENDA!\n");
		}
		menu(sistema);
	}
}