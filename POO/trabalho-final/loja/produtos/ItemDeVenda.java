package loja.produtos;

/**
* Interface que irá definir as ações dos items que serão vendidos na loja. A classe que implementa esta interface
* deverá conter o atributo valor do tipo double que deverá ser utilizado pelas funções {@link #setValor setValor} e {@link #getValor getValor},
* e o atributo vendido do tipo boolean que deverá ser utilizado pelas funções {@link #vender vender} e {@link #foiVendido foiVendido}.
*/
public interface ItemDeVenda {

	/**
	* Define o valor de venda do item à ser vendido.
	* @param valor Valor do item
	*/
	void setValor(double valor);

	/**
	* Recupera o valor de venda do item.
	* @return Valor de venda do item.
	*/
	double getValor();

	/**
	* Ação que corresponde a venda do item, irá marcar o item como vendido.
	*/
	void vender();

	/**
	* Verifica se o item foi vendido.
	* @return booleano que determina se o item foi vendido.
	*/
	boolean foiVendido();

}