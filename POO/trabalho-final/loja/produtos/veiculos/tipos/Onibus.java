package loja.produtos.veiculos.tipos;

import loja.produtos.veiculos.Veiculo;
import loja.produtos.ItemDeVenda;

import java.lang.StringBuilder;

/**
* Esta classe representa o tipo de Veiculo "Onibus", portanto extende da classe abstrata {@link loja.produtos.veiculos.Veiculo}
*/
public class Onibus extends Veiculo{

	int numAssentos;
	int numPortas;

	/**
	* Inicializará as características do veículo chamando o {@link loja.produtos.veiculos.Veiculo#Veiculo construtor}
	* da superclasse {@link loja.produtos.veiculos.Veiculo Veiculo} e inicializará também os seus próprios atributos. 
	* @param anoFabricacao Ano de fabricação do veículo
	* @param placa Placa do veículo
	* @param marca Marca do veículo
	* @param modelo modelo do veículo
	* @param quilometragem Quilometragem rodada do veículo.
	* @param cilindradas Cilindradas do veículo.
	* @param potenciaCV Potência em cavalos do veículo.
	* @param numMarchas Número de marchas que o veículo possui.
	* @param numAssentos Número de assentos que o carro possui.
	* @param numPortas Número de portas que o carro possui.
	*/
	public Onibus(int anoFabricacao,String placa,String marca,String modelo,double quilometragem,int cilindradas,int potenciaCV,int numMarchas, int numAssentos, int numPortas) {
		super(anoFabricacao, placa, marca, modelo, quilometragem, cilindradas, potenciaCV, numMarchas);
		this.numAssentos = numAssentos;
		this.numPortas = numPortas;
	}

	/**
	* @return Retorna a string que representa a classe {@link loja.produtos.veiculos.tipos.Onibus Onibus}, para isso utiliza o método {@link loja.produtos.veiculos.Veiculo#toString() toString} da superclasse Veiculo
	* e acrescenta os seus próprios atributos. 
	*/
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("ONIBUS\n");
		stringBuilder.append(super.toString());
		stringBuilder.append("Número Assentos...: " + this.numAssentos + "\n");
		stringBuilder.append("Número Portas.....: " + this.numPortas + "\n");

		return stringBuilder.toString();
	}

}