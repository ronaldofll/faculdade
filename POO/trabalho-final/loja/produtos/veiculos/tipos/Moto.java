package loja.produtos.veiculos.tipos;

import loja.produtos.veiculos.Veiculo;

/**
* Esta classe representa o tipo de Veiculo "Moto", portanto extende da classe abstrata {@link loja.produtos.veiculos.Veiculo}
*/
public class Moto extends Veiculo{

	/**
	* Inicializará as características do veículo chamando o {@link loja.produtos.veiculos.Veiculo#Veiculo construtor}
	* da superclasse {@link loja.produtos.veiculos.Veiculo Veiculo}.
	* @param anoFabricacao Ano de fabricação do veículo
	* @param placa Placa do veículo
	* @param marca Marca do veículo
	* @param modelo modelo do veículo
	* @param quilometragem Quilometragem rodada do veículo.
	* @param cilindradas Cilindradas do veículo.
	* @param potenciaCV Potência em cavalos do veículo.
	* @param numMarchas Número de marchas que o veículo possui.
	*/
	public Moto(int anoFabricacao,String placa,String marca,String modelo,double quilometragem,int cilindradas,int potenciaCV,int numMarchas) {
		super(anoFabricacao, placa, marca, modelo, quilometragem, cilindradas, potenciaCV, numMarchas);
	}

	/**
	* @return Retorna a string que representa a classe {@link loja.produtos.veiculos.tipos.Moto Moto}, para isso utiliza o método {@link loja.produtos.veiculos.Veiculo#toString() toString} da superclasse Veiculo
	* e acrescenta os seus próprios atributos. 
	*/
	@Override
	public String toString(){
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("MOTO\n");
		stringBuilder.append(super.toString());

		return stringBuilder.toString();
	}

}