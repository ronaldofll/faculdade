package loja.produtos.veiculos;

import loja.produtos.ItemDeVenda;

import java.lang.StringBuilder;

/**
* Classe abstrata que contém os dados comuns dos Veiculos que serão vendidos na loja,
* implementa {@link loja.produtos.ItemDeVenda ItemDeVenda} pois será um dos produtos que será vendido na loja.
*/
public abstract class Veiculo implements ItemDeVenda{

	/**
	* Ano em que o veículo foi fabricado.
	*/
	public int anoFabricacao;
	public String placa;
	public String marca;
	public String modelo;
	public double quilometragem;
	public int cilindradas;
	public int potenciaCV;
	public int numMarchas;
	private double valor = 0;
	private boolean vendido = false;

	/**
	* Inicializará todos os atributos desta classe.
	* @param anoFabricacao Ano de fabricação do veículo
	* @param placa Placa do veículo
	* @param marca Marca do veículo
	* @param modelo modelo do veículo
	* @param quilometragem Quilometragem rodada do veículo.
	* @param cilindradas Cilindradas do veículo.
	* @param potenciaCV Potência em cavalos do veículo.
	* @param numMarchas Número de marchas que o veículo possui.
	*/
	public Veiculo (int anoFabricacao,String placa,String marca,String modelo,double quilometragem,int cilindradas,int potenciaCV,int numMarchas) {
		this.anoFabricacao = anoFabricacao;
		this.placa = placa;
		this.marca = marca;
		this.modelo = modelo;
		this.quilometragem = quilometragem;
		this.cilindradas = cilindradas;
		this.potenciaCV = potenciaCV;
		this.numMarchas = numMarchas;
	}

	@Override
	public void setValor(double valor) {
		this.valor = valor;
	}

	@Override
	public double getValor() {
		return this.valor;
	}

	@Override
	public void vender() {
		this.vendido = true;
	}

	@Override 
	public boolean foiVendido() {
		return this.vendido;
	}

	/**
	* @return Retorna a string que representa a classe {@link loja.produtos.veiculos.Veiculo Veiculo}, retornando uma string com todos os valores dos atributos deste Veiculo.
	*/
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Ano...............: " + this.anoFabricacao + "\n");
		stringBuilder.append("Placa.............: " + this.placa + "\n");
		stringBuilder.append("Marca.............: " + this.marca + "\n");
		stringBuilder.append("Modelo............: " + this.modelo + "\n");
		stringBuilder.append("Quilometragem.....: " + this.quilometragem + "\n");
		stringBuilder.append("Cilindradas.......: " + this.cilindradas + "\n");
		stringBuilder.append("Potencia..........: " + this.potenciaCV + "cv\n");
		stringBuilder.append("Marchas...........: " + this.numMarchas + "\n");
		stringBuilder.append("Valor.............: " + this.valor + "\n");
		stringBuilder.append("Estado............: " + (this.vendido ? "Vendido" : "À venda") + "\n");

		return stringBuilder.toString();
	}


}
