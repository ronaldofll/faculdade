package loja.sistema;

import loja.produtos.veiculos.Veiculo;
import loja.produtos.ItemDeVenda;

import java.util.List;
import java.util.ArrayList;

/**
* Classe que irá gerenciar todo os produtos e vendas da loja.
*/
public class Sistema {

	private List<ItemDeVenda> produtosDaLoja = new ArrayList<ItemDeVenda>();
	private Double valorVendas = 0d;

	/**
	* Cadastra um novo veículo na lista de produtos da loja e o disponibiliza para venda atribuindo um valor.
	* @param veiculo Veículo a ser cadastrado para venda
	* @param valor de venda do Veículo.
	* @return retorna falso quando a placa do veículo já existe no cadastro, para informar que o cadastro não foi efetuado.
	*/
	public boolean cadastrarVeiculo(Veiculo veiculo, double valor) {
		boolean jaExiste = false;

		//Verifica se o veículo já está cadastrado antes de cadastrar.
		for(ItemDeVenda itemDeVenda : produtosDaLoja) {
			if(itemDeVenda instanceof Veiculo) {
				Veiculo veiculoDaLista = (Veiculo) itemDeVenda;
				if(veiculo.placa.equalsIgnoreCase(veiculoDaLista.placa)){
					return false;
				}
			}
		}

		if(veiculo instanceof ItemDeVenda) {
			ItemDeVenda itemDeVenda = (ItemDeVenda) veiculo;
			itemDeVenda.setValor(valor);
			produtosDaLoja.add(itemDeVenda);
		}

		return true;
	}

	/**
	* Lista todos os veículos na lista de produtos da loja que ainda não foram vendidos.
	*/
	public void listarVeiculosNaoVendidos() {
		System.out.println("\n\tVEÍCULOS NÃO VENDIDOS:");
		for(ItemDeVenda itemDeVenda : produtosDaLoja) {
			if(!itemDeVenda.foiVendido()) {
				if(itemDeVenda instanceof Veiculo) {
					Veiculo veiculo = (Veiculo) itemDeVenda;
					System.out.println(veiculo);
				}
			}
		}
	}

	/**
	* Lista os veículos na lista de produtos da loja(inclusive os já vendidos). Filtrando-os pelo ano de fabricação. 
	* @param minAnoFabricacao Ano mínimo de fabricacao que o veículo deve possuir para aparecer na lista.
	* @param maxAnoFabricacao Ano máximo de fabricacao que o veículo deve possuir para aparecer na lista.
	*/
	public void listarVeiculosPorAno(int minAnoFabricacao, int maxAnoFabricacao) {
		System.out.println("\n\tVEÍCULOS DE ANOS ENTRE ("+minAnoFabricacao+" - "+maxAnoFabricacao+"):");
		for(ItemDeVenda itemDeVenda : produtosDaLoja) {
			if(itemDeVenda instanceof Veiculo) {
				Veiculo veiculo = (Veiculo) itemDeVenda;
				if(veiculo.anoFabricacao >= minAnoFabricacao && veiculo.anoFabricacao <= maxAnoFabricacao) {
					System.out.println(veiculo);
				}
			}
		}
	}

	/**
	* Lista os veículos na lista de produtos da loja(inclusive os já vendidos). Filtrando-os pelo ano de fabricação. 
	* @param anoFabricacao Ano exato de fabricacao que o veículo deve possuir para aparecer na lista.
	*/
	public void listarVeiculosPorAno(int anoFabricacao) {		
		listarVeiculosPorAno(anoFabricacao, anoFabricacao);
	}

	/**
	* Lista os veículos na lista de produtos da loja(inclusive os já vendidos). Filtrando-os pelo modelo.
	* @param modelo Modelo que o veículo deve possuir para aparecer na lista. 
	*/
	public void listarVeiculosPorModelo(String modelo) {
		System.out.println("\n\tVEÍCULOS DO MODELO "+modelo+":");		
		List<Veiculo> resultado = new ArrayList<Veiculo>();
		for(ItemDeVenda itemDeVenda : produtosDaLoja) {
			if(itemDeVenda instanceof Veiculo) {
				Veiculo veiculo = (Veiculo) itemDeVenda;
				if(veiculo.modelo != null && veiculo.modelo.equalsIgnoreCase(modelo)) {
					System.out.println(veiculo);
				}
			}
		}
	}

	/**
	* Lista os veículos na lista de produtos da loja(inclusive os já vendidos). Filtrando-os pela marca.
	* @param marca Marca que o veículo deve possuir para aparecer na lista. 
	*/
	public void listarVeiculosPorMarca(String marca) {
		System.out.println("\n\tVEÍCULOS DE MARCA "+marca+":");		
		for(ItemDeVenda itemDeVenda : produtosDaLoja) {
			if(itemDeVenda instanceof Veiculo) {
				Veiculo veiculo = (Veiculo) itemDeVenda;
				if(veiculo.marca != null && veiculo.marca.equalsIgnoreCase(marca)) {
					System.out.println(veiculo);
				}
			}
		}
	}

	/**
	* Lista os veículos na lista de produtos da loja(inclusive os já vendidos). Filtrando-os pela quilometragem rodada.
	* @param minQuilometragem Quilometragem mínima que o veículo deve possuir para aparecer na lista.
	* @param maxQuilometragem Quilometragem máxima que o veículo deve possuir para aparecer na lista.
	*/
	public void listarVeiculosPorQuilometragem(int minQuilometragem, int maxQuilometragem) {
		System.out.println("\n\tVEÍCULOS DE QUILOMETRAGEM ENTRE ("+minQuilometragem+" - "+maxQuilometragem+"):");
		List<Veiculo> resultado = new ArrayList<Veiculo>();
		for(ItemDeVenda itemDeVenda : produtosDaLoja) {
			if(itemDeVenda instanceof Veiculo) {
				Veiculo veiculo = (Veiculo) itemDeVenda;
				if(veiculo.quilometragem >= minQuilometragem && veiculo.quilometragem <= maxQuilometragem) {
					System.out.println(veiculo);
				}
			}
		}
	}

	/**
	* Lista os veículos na lista de produtos da loja(inclusive os já vendidos). Filtrando-os pela quilometragem. 
	* @param quilometragem Quilometragem exata que o veículo deve possuir para aparecer na lista.
	*/
	public void listarVeiculosPorQuilometragem(int quilometragem) {
		listarVeiculosPorQuilometragem(quilometragem, quilometragem);
	}

	/**
	* Procura pelo veículo através da placa e o marca como vendido, também aumenta o valor das vendas de acordo com o valor do veículo.
	* @param placa Placa do veículo que será vendido.
	* @return Retorna um booleano que representa se o veículo foi encontrado e a venda foi efetuada com sucesso.
	*/
	public boolean venderVeiculo(String placa) {
		for(ItemDeVenda itemDeVenda : produtosDaLoja) {
			if(itemDeVenda instanceof Veiculo){
				Veiculo veiculo = (Veiculo) itemDeVenda;
				if(!veiculo.foiVendido()) {
					if(veiculo.placa.equalsIgnoreCase(placa)) {
						itemDeVenda.vender();
						valorVendas += itemDeVenda.getValor();
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	* Mostra o valor de vendas efetuadas.
	*/
	public void lerValorDeVendas() {
		System.out.println("\nValor vendas: " + valorVendas + "\n");
	}
}