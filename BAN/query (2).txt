/*
Buscar os dados dos pacientes que estão com ‘sarampo’
Resposta: σ doenca='sarampo' (σ Consultas.codp=Pacientes.codp (Consultas⨯Pacientes))
• Buscar os dados dos médicos ‘Ortopedista’ com mais de
55 anos
Resposta: σ idade>55 ∧especialidade='Ortopedista' Medicos
• Buscar os dados de todas as consultas, exceto aquelas
marcadas para os médicos CRM 102401 e 100985
Resposta: σ Consultas.codm!=102401 ∧ Consultas.codm!=100985 Consultas
• Buscar os dados dos ambulatórios do quarto andar. Estes
ambulatórios devem ter capacidade superior ou igual a
50 ou o número do ambulatório deve ser superior a 410
Resposta: σ andar=4 ∧ (capacidade≥50 ∨ nroa>410) Ambulatorios
*/




