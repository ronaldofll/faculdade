#!/bin/bash

# 2.Elabore um programa (VerificaPrograma.sh) em shell script no qual o usuário passa por 
#   parâmetro o nome do processo e o programa verifica se o processo está sendo executado ou não. 
#   Caso o porcesso esteja sendo executado mostre o PID e o estado no qual o processo se encontra  
#   na tela. Caso não esteja sendo executado emitir uma mensagem na tela de “processo não 
#   encontrado”

NOMEPROCESSO=$1

if [ -z $NOMEPROCESSO ]; then
	echo "Não foi informado o nome do processo."
else
	
	PROCESSO=$(ps -aux | grep $NOMEPROCESSO)
	
	if [ -z PROCESSO ]; then
		echo "Processo não encontrado"
	else
		PID=$(ps -au | grep $NOMEPROCESSO | head -n 1 | awk '{print $2}')
		STAT=$(ps -au | grep $NOMEPROCESSO | head -n 1 | awk '{print $8}')
		NOMEPROCESSO=$(ps -au | grep $NOMEPROCESSO | head -n 1 | awk '{print $13}')
		echo "$NOMEPROCESSO $PID $STAT"
	fi
fi


