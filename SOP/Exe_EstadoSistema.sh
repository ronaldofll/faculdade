#!/bin/bash

#3. Elabore um programa em shellscript (EstadoSistema.sh) no qual o programa grava um arquivo txt
#com as seguintes informações (nesta sequência):
#a) Número total de processos ativos no sistema.
#b) Número de processos ativos do usuário corrente.
#c) Nome do usuário corrente.
#d) Quantidade de processos sendo executados como root.
#Obs. O arquivo deve ter o nome no seguinte formato: 
#<AAAAMMDD>_<HH:MM:SS>_EstadoSistema_<usuário corrente>.txt

QTDPROCESSOS=$(ps aux | wc -l)
QTDPROCUSUARIO=$(ps -f -u $(whoami) | wc -l)
USUARIO=$(whoami)
QTDPROCESSOROOT=$(ps -f -u root | wc -l)

DATA=$(date +%Y%m%d_%H:%M:%S)

NOMEARQUIVO=$(printf "%s_EstadoSistema_%s.txt" $DATA $USUARIO)



echo "QTD PROCESSOS: $(($QTDPROCESSOS-2))" >> $NOMEARQUIVO
echo "QTD PROCESSOS USUÁRIO: $(($QTDPROCUSUARIO-2))" >> $NOMEARQUIVO
echo "Nome usuario: $USUARIO" >> $NOMEARQUIVO
echo "QTD PROCESSOS ROOT: $(($QTDPROCESSOROOT-2))" >> $NOMEARQUIVO
