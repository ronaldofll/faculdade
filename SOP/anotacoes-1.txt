
Sistemas Operacionais: conjunto de módulos de software

FUNÇÕES
	Administrar recursos do sistema
	Oferecer interface de alto nível
	Diminuir interação programador x maquina
	Gerenciamento hardware

Concorrência: execução paralela de processos no sistema

Compartilhamento: Gerenciamento do uso de recursos que serão utilizados por mais de 1 processo

SISTEMAS MONOLÍTICOS
	Neste tipo de sistema existem o modo usuário(Aplicações) e núcleo e as interações entre as aplicações e o sistema são feitas através de 
	chamadas à modulos que são executados no núcleo. Não possui uma estrutura bem definida de núcleo.

	Problemas:
		Um módulo com problemas pode afetar todo o sistema.

SISTEMAS EM CAMADAS
	Sistema baseado em hierarquia de camadas

SISTEMAS MICRO NÚCLEO
	Algumas funções do núcleo são delegadas á processos ao nível de usuário
	Cada processo é responsável por gerenciar serviços específicos.
	Aplicações fazem chamadas a estes serviços para utilizar recursos do núcleo.
	Maior facilidade de manutenção

MAQUINA VIRTUAL
	Simula ambiente de computação física dentro de um sistema operacional

	
O núcleo Linux por adotar uma arquitetura monolítica teve a sua depuração facilitada 
tornando possível uma versão mais estável e consequentemente novas releases em ciclos mais regulares. 
Por outro lado, o GNU/HURD adota uma arquitetura de micro-núcleo que torna o desenvolvimento e depuração mais demoroso e complexo, 
consequentemente o GNU/HURD tem ciclos de lançamento inicial mais lento e atualizações mais rápidas.