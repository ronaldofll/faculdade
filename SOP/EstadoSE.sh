#!/bin/bash
# AP2-Q2

NOMEARQUIVO=$(echo $(date +%Y%m%d_%H:%M:%S)_EstadoES_$(whoami).txt)

QTDCPU=$(lscpu | grep -e '^CPU(s):' | awk {'print $2'})
 

echo "TOTAL PROCESSADORES: $QTDCPU" >> $NOMEARQUIVO

QTDNUCLEO=$(grep -e "^processor" /proc/cpuinfo | wc -l)
echo "QUANTIDADE DE NUCLEOS: $QTDNUCLEO" >> $NOMEARQUIVO

MODELO=$(cat /proc/cpuinfo | grep 'model name' | uniq | cut -d: -f 2)
echo "MODELO DA CPU: $MODELO" >> $NOMEARQUIVO
 

MEMORIA=$(grep 'MemTotal' /proc/meminfo | cut -d' ' -f8-9)
echo "QTD MEMORIA PRINCIPAL: $MEMORIA" >> $NOMEARQUIVO

QTDHD=$(lsblk -d | tail -n +2 | wc -l)
echo "QTD HD: $QTDHD" >> $NOMEARQUIVO

MEMORIAHD=$(lsblk -d | tail -n +2 | cut -d' ' -f1,10)
echo "" >> $NOMEARQUIVO
echo "MEMORIA HD: $MEMORIAHD" >> $NOMEARQUIVO
 

echo "" >> $NOMEARQUIVO
echo "VELOCIDADE DA INTERFACE DE REDE:" >> $NOMEARQUIVO
for rede in $(ls /sys/class/net); do 
      velocidade=$(cat /sys/class/net/$rede/statistics/rx_bytes) 
      echo "$rede - $velocidade" >> $NOMEARQUIVO
done
