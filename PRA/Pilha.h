#define ERRO_PILHA_CHEIA -1
#define ERRO_PILHA_VAZIA -2
typedef struct
{
    void **dados;
    int capacidade;
    int topo;
    int tamInfo;
} Pilha;

void inicializa_pilha(Pilha *p, int c, int t);
int empilha(Pilha *p, void *info);
int desempilha(Pilha *p, char *info);
int pilha_vazia(Pilha p);
int pilha_cheia(Pilha p);
void desaloca_pilha(Pilha *p);
