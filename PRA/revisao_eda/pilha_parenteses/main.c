#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pilha.h"
int main ()
{
    Pilha p1;
    char expressao[50] = {0};
    int qtdeCaracter, i;

    printf("Digite sua expressao aritmetica: ");
    scanf("%s", expressao);

    qtdeCaracter = strlen(expressao);

    inicializa_pilha(&p1, qtdeCaracter, sizeof(char));

    for (i = 0; i < qtdeCaracter; i++)
    {
        if (expressao[i] == '(' || expressao[i] == '{' || expressao[i] == '[') {
            empilha(&p1, &expressao[i]);   
        }

        if (expressao[i] == ')' || expressao[i] == '}' || expressao[i] == ']') {
            char desempilhado;
            desempilha(&p1, &desempilhado);

            if(expressao[i] == ')' && '(' != desempilhado) {
                printf("INCORRETO!\n");
                return 0;
            }

            if(expressao[i] == '}' && '{' != desempilhado) {
                printf("INCORRETO!\n");
                return 0;
            }
            
            if(expressao[i] == ']' && '[' != desempilhado) {
                printf("INCORRETO!\n");
                return 0;
            }
        }
    }

    if (pilha_vazia(p1) == 0)
        printf("INCORRETO!\n");
    else
        printf("CORRETO!\n");

    desaloca_pilha(&p1); // Desaloco a pilha | Libero a mem�ria

    return 0;
}
