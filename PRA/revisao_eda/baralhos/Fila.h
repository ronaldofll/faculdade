
#define ERRO_FILA_VAZIA -1;
#define ERRO_FILA_CHEIA -2;

typedef struct {
	int* dados;
	int capacidade;
	int inicio;
	int fim;
	int quantidade;
}Fila;

void inicializaFila(Fila* f, int capacidade);

int inserir(Fila* f, int x);

void mostra(Fila f);

int remover(Fila* f, int* x);

void desaloca(Fila* f);

int filaCheia(Fila f);

int filaVazia(Fila f);

int quantidade(Fila f);
