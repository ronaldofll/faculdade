#include <stdio.h>
#include <stdlib.h>
#include "Fila.h"

void operacao(Fila *baralho, Fila *descartados) {
	int temp;
	remover(baralho, &temp);
	inserir(descartados, temp);

	remover(baralho, &temp);
	inserir(baralho, temp);
}

int main(){

	int n = 0;
	do{
		printf("\nQuantidade no baralho: ");
		scanf("%d", &n);
	} while(n > 50);

	Fila baralho, descartados;
	inicializaFila(&baralho, n);
	inicializaFila(&descartados, n-1);

	int i;
	for(i = 1; i <= n; i++) {
		inserir(&baralho, i);
	}

	while(quantidade(baralho) > 1) {
		operacao(&baralho, &descartados);
	}

	printf("Descarte: ");
	mostra(descartados);
	printf("Restou: ");
	mostra(baralho);

	desaloca(&baralho);
	desaloca(&descartados);

	return 0;
}


