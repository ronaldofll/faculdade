#include "Random.h"
#include <stdlib.h>

int gerarAleatorio(int min, int max) {
    int range, result, cutoff;

    if (min >= max)
        return min;
    range = max - min + 1;
    cutoff = (RAND_MAX / range) * range;

    do
    {
        result = rand();
    } while (result >= cutoff);

    return result % range + min;
}