#include "listaDuplamenteEncadeada.h"
#include "Random.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	ListaDupla l1;

	inicializaLista(&l1);

    srand(time(NULL));
    int qtd = gerarAleatorio(5,100);

    int i;
    for(i = 0; i < qtd; i++) {
        int valor = gerarAleatorio(0, 100);
        insereNoFim(&l1, &valor);
    }

    printf("\nDados na Lista:\n");
    mostraLista(l1);

    printf("\nDados na Lista (invertido):\n");
    mostraListaInvertida(l1);

    removePares(&l1);

    printf("\nApós remoção dos pares:\n");
    mostraLista(l1);

    desalocaLista(&l1);

    return 0;
}