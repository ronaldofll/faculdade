
#define ERRO_LISTA_VAZIA -1
#define ERRO_POS_INVALIDA -2

typedef struct elemDuplo {
	int* info;
	struct elemDuplo* ant;
	struct elemDuplo* suc;
}ElementoDuplo;

typedef struct{
	struct elemDuplo* cabeca;//Elemento* cabeca;
}ListaDupla;


void inicializaLista(ListaDupla* l1);

int insereNoInicio(ListaDupla* l1, int* x);

int insereNoFim(ListaDupla *l, int *valor);

ElementoDuplo* alocaElemento(int* valor);

void mostraLista(ListaDupla l1);

void mostraListaInvertida(ListaDupla l1);

void removePares(ListaDupla *l1);

int desalocaLista(ListaDupla *l);