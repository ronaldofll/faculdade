#include "listaDuplamenteEncadeada.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h> 


void inicializaLista(ListaDupla* l1) {
	l1->cabeca = NULL;
}

int listaVazia(ListaDupla l1){
	return l1.cabeca == NULL;
}

ElementoDuplo* alocaElemento(int* valor) {
	ElementoDuplo* p = malloc(sizeof(ElementoDuplo));
	if(p == NULL){
		return NULL;
	}
	p->info = malloc(sizeof(int));
	if(p->info == NULL){
		free(p);
		return NULL;
	}
	memcpy(p->info, valor, sizeof(int));

	return p;
}

int insereNoInicio(ListaDupla* l1, int* valor) {
	ElementoDuplo* p = alocaElemento(valor);
	if(p==NULL){
		return 0;
	}
	p->suc = l1->cabeca;
	l1->cabeca = p;
	if(p->suc != NULL)
		p->suc->ant = p;
	return 1;
}

int insereNoFim(ListaDupla *l, int *valor) // O *l recebe o endereço de memória do l1 da main
{
    if(listaVazia(*l))
        return insereNoInicio(l, valor); //Passo o endereço de memória do l1 que está na main. Se eu passar o &l, estaria passando o endereço de memória do l do InsereNofim

    ElementoDuplo *novo = alocaElemento(valor);

    if (novo == NULL) {
    	printf("\nERRO\n");
        return 0; //deu erro na alocacao dentro de aloca_elemento e retorno 0, erro na operação
    }

    ElementoDuplo *p = l->cabeca;

    while(p->suc != NULL)
    {
        p = p->suc; //p aponta para o endereço de memória do próximo elemento até chegar no último, que tem o próximo null
    }

    p->suc = novo; // Qdo sair do while, o p->proximo armazena o endereço do novo elemento

    novo->suc = NULL; // O próximo do último elemento, deve ser NULL

    novo->ant = p;

    return 1;
}

void mostraLista(ListaDupla l1) {
	if(listaVazia(l1)){
		printf("Lista vazia\n");
	}
	else{
		ElementoDuplo *p = l1.cabeca;

		while(p!=NULL){
			printf("%d\n", *p->info);
			p = p->suc;
		}
	}

}

void mostraListaInvertida(ListaDupla l1) {

	if(listaVazia(l1)){
		printf("Lista Dupla vazia\n");
	}
	else{
		ElementoDuplo *p = l1.cabeca;

		while(p->suc != NULL){
			p = p->suc;
		}
		while(p!=NULL){
			printf("%d\n", *p->info);
			p = p->ant;
		}
	}
}

void removePares(ListaDupla *l1) {
	if(listaVazia(*l1)) {
		return;
	}

	ElementoDuplo *p = l1->cabeca;

	do {

		ElementoDuplo *aux = p->suc;
		ElementoDuplo *anterior = p->ant;
		ElementoDuplo *proximo = p->suc;
		if(((*(p->info)) % 2) == 0) {

			if(proximo != NULL) {
				proximo->ant = anterior;
			}

			//printf("%p\n", anterior);
			if(anterior != NULL) {
				anterior->suc = proximo;
			}

			if(l1->cabeca == p) {
				l1->cabeca = aux;
			}

			free(p->info);
			free(p);
		}

		p = aux;
	}while(p != NULL);
}


int desalocaLista(ListaDupla *l)
{
    if(!listaVazia(*l))
    {
        ElementoDuplo *p = l->cabeca;
        ElementoDuplo *pAux;

        while(p->suc != NULL)
        {
            pAux = p->suc;
            free(p->info);
            free(p);
            p = pAux;
        }

        free(p->info);
        free(p);
        l->cabeca = NULL;

        return 1;
    }

    return 0;
}