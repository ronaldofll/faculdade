//cd /home/ronaldo/workspace/git/faculdade/PRA/trabalho2 && clear && gcc main.c Arvore.c Aluno.c -o main.e && ./main.e

#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include "Aluno.h"
#include "Arvore.h"

#define NOME_ARQUIVO "arquivos/arquivo.bin"
#define NOME_ARQUIVO_ORDENADO "arquivos/arquivo-ordenado.bin"
#define TEMP_DIR "arquivos/temp"
#define TEMP_FILE "temp"
#define BIN ".bin"

int comparaAlunoPorNome( const void *x, const void *y );
void ordenaAlunos(Aluno* alunos, int qtd, int (*compara)(const void*, const void*));

char* dirFile(char* fileName, int* num) {
    char* dirFile = malloc(sizeof(char) * 50);

    char barra[] = "/";

    if(num == NULL) {
        sprintf(dirFile, "%s%s%s", TEMP_DIR, barra, fileName);
    } else {
        sprintf(dirFile, "%s%s%s%d", TEMP_DIR, barra, fileName, *num);
    }

    return dirFile;
}

/*Salva um array de alunos*/
void gravarAlunos(FILE *fp, Aluno *alunos, int qtd) {
    fwrite(alunos, sizeof(Aluno), qtd, fp);
}

/*Criar arquivo temporário e salva um array de alunos*/
int salvaBlocoOrdenado(Aluno* alunos, int qtd) {

    static int cont = 0;
    cont++;
    char *tempFile = dirFile(TEMP_FILE, &cont);

    FILE *fp = fopen(tempFile, "wb+");
    if(fp == NULL) {
        printf("\nErro na abertura do arquivo %s", tempFile);
        return 0;
    }

    ordenaAlunos(alunos, qtd, comparaAlunoPorNome);

    gravarAlunos(fp, alunos, qtd);

    fclose(fp);

    return 1;
}

/*Divide um arquivo inicial em vários blocos ordenados*/
int ordenarBlocos(FILE *fp, int tamEmBytesDoBloco) {

    fseek(fp, 0, SEEK_END);
    int qtdAlunos = ftell(fp) / sizeof(Aluno);

    fseek(fp, 0, SEEK_SET);

    int qtdPorBloco = tamEmBytesDoBloco / sizeof(Aluno);

    int qtdBlocos;
    if(qtdAlunos < qtdPorBloco) {
        qtdBlocos = 1;
        qtdPorBloco = qtdAlunos;
    } else {
        qtdBlocos = qtdAlunos/qtdPorBloco;
    }

    int i;
    for(i = 0; i < qtdBlocos; i++) {

        Aluno *alunos = malloc(sizeof(Aluno) * qtdPorBloco);
        fread(alunos, sizeof(Aluno), qtdPorBloco, fp);

        salvaBlocoOrdenado(alunos, qtdPorBloco);

        free(alunos);
    }

    return 1;
}

void mostraAluno(void* x) {
    Aluno* aluno = (Aluno*) x;
    printf("%s,", aluno->nome);
}

void salvarParaArquivoOrdenado(Arvore* arvore) {
    int i;

    Aluno* alunos = malloc(arvore->qtd * sizeof(Aluno));

    int qtd = arvore->qtd;
    for(i = 0; i < qtd; i++) {
        int e = removeNo(arvore, &alunos[i]);
    }

    FILE* fp = fopen(NOME_ARQUIVO_ORDENADO, "ab+");
    fwrite(alunos, sizeof(Aluno), qtd, fp);

    free(alunos);

    fclose(fp);
}

int contarArquivos(DIR *directory) {
    int cont = 0;

    struct dirent* file;

    while ((file=readdir(directory)) != NULL) {
        if( strcmp(file->d_name, ".") != 0 && strcmp(file->d_name, "..") != 0 ) {
            cont++;
        }
    }
    rewinddir(directory);
    return cont;
}

void lerAlunosERemoverDoArquivo (char* fileName, void* alunos, int qtd) {

    char* fileDir = dirFile(fileName, NULL);

    FILE* fp = fopen(fileDir, "r+");

    fseek(fp, 0, SEEK_END);
    int tam = ftell(fp);

    if(tam == 0) {
        return;
    }

    int qtdNoArquivo = tam/sizeof(Aluno);
    int qtdRestante;

    if(qtdNoArquivo < qtd) {
        qtdRestante = 0;
        qtd = qtdNoArquivo;
    } else {
        qtdRestante = (tam/sizeof(Aluno))-qtd;
    }

    fseek(fp, 0, SEEK_SET);

    fread(alunos, sizeof(Aluno), qtd, fp);

    remove(fileDir);

    if(qtdRestante > 0) {
        Aluno* alunosRestantes = malloc(sizeof(Aluno) * qtdRestante);
        fread(alunosRestantes, sizeof(Aluno), qtdRestante, fp);

        FILE* novoArquivo = fopen(fileDir, "wb+");
        gravarAlunos(novoArquivo, alunosRestantes, qtdRestante);
        fclose(novoArquivo);

        free(alunosRestantes);
    }

    fclose(fp);
}

void intercalar(int qtdMemoriaEmBytes, int(*compara)(const void*, const void*)) {

    DIR *directory;
    struct dirent* file;
    directory = opendir(TEMP_DIR);

    int qtdArquivos = contarArquivos(directory);
    int qtdMemoria = qtdMemoriaEmBytes/(qtdArquivos+1);
    int qtdPorVez = qtdMemoria/sizeof(Aluno);

    Arvore arvore;
    inicializaArvore(&arvore, sizeof(Aluno), qtdPorVez);

    while(qtdArquivos > 0) {
        void *blocos = malloc(sizeof(Aluno) * qtdArquivos * qtdPorVez);

        int i = 0;
        while ((file=readdir(directory)) != NULL) {

            if( strcmp(file->d_name, ".") != 0 && strcmp(file->d_name, "..") != 0 ) {

                printf("\nFile: temp%d\n", i);

                clock_t c1 = clock();
                lerAlunosERemoverDoArquivo(file->d_name, (blocos + (sizeof(Aluno) * qtdPorVez * i)), qtdPorVez);
                clock_t c2 = clock();
                printf("\nOperação concluída em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
            }

            i++;
        }

        int j, k;
        for(j = 0; j < qtdArquivos; j++) {

            clock_t c1 = clock();

            printf("\n\nLendo arquivo %d\n", j+1);
            Aluno *alunos = (Aluno*)(blocos + (sizeof(Aluno) * (j*qtdPorVez)));
            for(k = 0; k < qtdPorVez; k++) {
                //printf("%d - %d\n", k, qtdPorVez);
                Aluno aluno = alunos[k];
                if(&aluno == NULL || strcmp(aluno.nome, "") == 0) {
                    break;
                }

                //printf("\n%s", aluno.nome);

                int ret = insereNo(&arvore, &aluno, compara);
                if(ret == ERRO_CAPACIDADE_ATINGIDA) {
                    //printf("Quantidade na arvore antes: %d\n", arvore.qtd);
                    salvarParaArquivoOrdenado(&arvore);

                    //printf("Quantidade na arvore depois: %d\n", arvore.qtd);
                    ret = insereNo(&arvore, &aluno, compara);
                }
            }

            clock_t c2 = clock();
            printf("\n\nOperação concluída em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
        }

        rewinddir(directory);
        qtdArquivos = contarArquivos(directory);

        free(blocos);
    }
    if(arvore.qtd > 0) {
        salvarParaArquivoOrdenado(&arvore);
    }

    closedir(directory);
}

int main() {
    FILE *f = fopen(NOME_ARQUIVO, "rb");
    if (f == NULL) {
        printf("Erro, arquivo \"%s\" não encontrado", NOME_ARQUIVO);
        return 1;
    }
    ordenarBlocos(f, 10000000);
    //ordenarBlocos(f, 100000000);

    remove(NOME_ARQUIVO_ORDENADO);
    intercalar(50000000, comparaAlunoPorNome);
    //intercalar(1000000000, comparaAlunoPorNome);

    fclose(f);

    return 1;
}

int comparaAlunoPorNome( const void *x, const void *y ) {
    Aluno *alunox = (Aluno*) x;
    Aluno *alunoy = (Aluno*) y;

    return strcmp(alunox->nome, alunoy->nome);
}

void ordenaAlunos(Aluno* alunos, int qtd, int (*compara)(const void*, const void*)) {
    if(alunos == NULL) {
        return;
    }

    qsort(alunos, qtd, sizeof(Aluno), compara);
}