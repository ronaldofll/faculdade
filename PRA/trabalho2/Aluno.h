#include <stdio.h>

typedef struct a
{
    long matricula;
    char nome[32];
    char dataNascimento[11];
    char disciplinas[10][60];
    int qtdDisciplinas;
} Aluno;

Aluno gerarAluno();

void lerPosicao(FILE *fp, long pos);

void lerTodosAlunos();

void mostrarPaginado(int pg, int qtd);

Aluno* lerPagina(int pg, int qtd);

void mostrarAluno(Aluno aluno);

void cadastrarAlunosPorTamanhoArquivo(long tamEmKbs);

void cadastrarAlunosPorPaginacao(int paginas, int quantidadePorPagina);

void limparRegistroAlunos();