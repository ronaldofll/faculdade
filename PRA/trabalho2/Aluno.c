#include "Aluno.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define NOME_ARQUIVO "arquivos/arquivo.bin"
#define QTD_DISCIPLINAS 30

int ultMatricula = 0;

char disciplinas[QTD_DISCIPLINAS][10] = {"AGT0001","ALGA001","CDI0001","MCI0001","TGA0002","AOC0002","EST0006","LPG0002","MAT0002","TGS0002","ANA1001","EDA0001","FSI0002","GFC0001","POO0001","ANA2001","BAN1001","PRA0001","SOFT002","SOP0002","BAN2001","ECS1004","EMI0002","PES0002","REC0002","ATC0009","DIR0002","ECS2004","ETI0002","GPR0002"};

void lerPosicao(FILE *fp, long pos) {

    fseek(fp, 0, SEEK_END);
    long qtdTotal = ftell(fp)/sizeof(Aluno);

    if(pos >= qtdTotal) {
        return;
    }

    Aluno aluno;
    fseek(fp, pos*(sizeof(Aluno)), SEEK_SET);
    fread(&aluno, 1, sizeof(Aluno), fp);
    mostrarAluno(aluno);
}

void lerTodosAlunos() {

    FILE *fp = fopen(NOME_ARQUIVO, "rb");
    if(fp == NULL) {
        printf("\n\nNão foi possível abrir o arquivo!\n");
        return;
    }

    fseek(fp, 0, SEEK_END);
    int qtd = ftell(fp) / sizeof(Aluno);

    if(qtd == 0) {
        printf("\nNenhum registro\n");
        return;
    }

    printf("\n%-10s|%-32s|%-50s|\n", "MATRICULA", "NOME", "DISCIPLINAS");
    printf("%s-%s-%s-\n", "----------", "--------------------------------", "--------------------------------------------------");

    fseek(fp, 0, SEEK_SET);
    while(ftell(fp) < qtd * sizeof(Aluno)) {
        Aluno aluno;
        fread(&aluno, 1, sizeof(Aluno), fp);
        mostrarAluno(aluno);
    }
    printf("%s-%s-%s-\n", "----------", "--------------------------------", "--------------------------------------------------");

    fclose(fp);
}

void mostrarPaginado(int pg, int qtd) {

    printf("%-10s|%-32s|%-50s|\n", "MATRICULA", "NOME", "DISCIPLINAS");

    Aluno *alunos = lerPagina(pg, qtd);

    int i;
    for(i = 0; i < qtd; i++) {
        mostrarAluno(alunos[i]);
    }
}

Aluno* lerPagina(int pg, int qtd) {
    FILE *fp = fopen(NOME_ARQUIVO, "rb");
    if(fp == NULL) {
        printf("\n\nNão foi possível abrir o arquivo!\n");
        return NULL;
    }

    if(qtd == 0) {
        return NULL;
    }

    fseek(fp, 0, SEEK_END);
    int qtdTotal = ftell(fp) / sizeof(Aluno);

    if(qtdTotal == 0) {
        printf("\nNenhum registro\n");
        return NULL;
    }

    int inicio = (pg-1)*qtd;

    if((inicio + qtd) > qtdTotal) {
        return NULL;
    }

    fseek(fp, inicio * sizeof(Aluno), SEEK_SET);

    Aluno *alunos = malloc(sizeof(Aluno) * qtd);
    fread(alunos, qtd, sizeof(Aluno), fp);

    fclose(fp);

    return alunos;
}

void mostrarAluno(Aluno aluno) {
    char matricula[10], disciplinas[50] = "";
    sprintf(matricula, "%09li", aluno.matricula);
    int i;
    for(i=0; i < aluno.qtdDisciplinas; i++) {
        if(i > 0) {
            strcat(disciplinas, ",");
        }
        strcat(disciplinas, aluno.disciplinas[i]);
    }


    printf("%-10s|%-32s|%-50s|", matricula, aluno.nome, disciplinas);
    printf("\n");
}