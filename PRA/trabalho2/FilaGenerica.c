#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "FilaGenerica.h"


void inicializaFila(FilaGenerica* f, int capacidade, int tamInfo) {
	f->dados = malloc(tamInfo * capacidade);
	f->inicio = 0;
	f->fim = 0;
	f->capacidade = capacidade;
	f->tamInfo = tamInfo;
	f->qtd = 0;
}

int inserir(FilaGenerica* f, void* info) {
	if(filaCheia(*f)){
		return ERRO_FILA_CHEIA;
	}

	f->dados[f->fim] = malloc(f->tamInfo);
	memcpy(f->dados[f->fim], info, f->tamInfo);
	f->qtd++;
	if(f->fim == f->capacidade-1){
		f->fim = 0;
	}else{
		f->fim++;
	}

	return 1;
}

int remover(FilaGenerica* f, void* destino) {
	if(filaVazia(*f)){
		return ERRO_FILA_VAZIA;
	}else{

		memcpy(f->dados[f->inicio], destino, f->tamInfo);
		free(f->dados[f->inicio]);
		f->qtd--;
		if(f->inicio == f->capacidade-1){
			f->inicio = 0;
		}else{
			f->inicio++;
		}

	}
}

void mostra(FilaGenerica f, void(*mostra)(void*)) {
	if(filaVazia(f)){
		printf("\nFila vazia!\n");
	}else{
		printf("\nFila Generica:\n");
		int i = f.inicio;
		while(i != f.fim) {
			mostra(f.dados[i]);
			if(i == f.capacidade-1){
				i = 0;
			}else{
				i++;
			}
		}
	}


}

void desaloca(FilaGenerica* f) {
	int i;
	for(i = 0; i < f->capacidade-1; i++){
		free(f->dados[i]);
	}

	free(f->dados);
}

int filaCheia(FilaGenerica f) {
	if(f.qtd == f.capacidade){
		return 1;
	}else {
		return 0;
	}
}

int filaVazia(FilaGenerica f) {
	if(f.qtd == 0) {
		return 1;
	}else{
		return 0;
	}
}
