//clear && gcc main.c Aluno.c Arvore.c Ordenacao.c Random.c -lm -o main.e && ./main.e
#include "Ordenacao.h"
#include "Aluno.h"
#include <string.h>

int comparaAlunoPorMatricula( const void *x, const void *y );
int comparaAlunoPorNome( const void *x, const void *y );
int comparaAlunoPorDataNascimento( const void *x, const void *y );
void ordenaAlunos(void* info, int qtd, int(*compara)(const void*, const void*));

void menu();
void menuCadastrarAlunos();
void menuExibirAlunos();
void menuOrdenarAlunos();

void ordenarAlunos(int(*compara)(const void*, const void*));

int main() {
    menu();
    return 1;
}

void menu() {
    printf("\n");
    printf("1 - Cadastrar alunos\n");
    printf("2 - Exibir alunos\n");
    printf("3 - Ordenar alunos\n");
    printf("4 - Sair\n");
    printf("\nEscolha uma das opcoes acima: ");

    char opt[10];
    scanf("%s", opt);

    if(strcmp(opt, "1") == 0) {
        menuCadastrarAlunos();
    } else if(strcmp(opt, "2") == 0) {
        menuExibirAlunos();
    } else if(strcmp(opt, "3") == 0) {
        menuOrdenarAlunos();
    } else if(strcmp(opt, "4") != 0) {
        printf("\nOpcao invalida!\n");
        menu();
    }
}

void menuCadastrarAlunos() {
    printf("\n");
    printf("1 - Por tamanho do arquivo\n");
    printf("2 - Por paginacao\n");
    printf("\nEscolha o modo de cadastramento:");

    char opt[10];
    scanf("%s", opt);

    if(strcmp(opt, "1") == 0 ) {
        printf("\nDigite o tamanho do arquivo: ");
        long tamArquivo;
        scanf("%li", &tamArquivo);
        clock_t c1 = clock();
        cadastrarAlunosPorTamanhoArquivo(tamArquivo);
        clock_t c2 = clock();
        printf("\nOperacao concluida em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
    } else if(strcmp(opt, "2") == 0) {
        printf("\nDigite a quantidade de paginas: ");
        int paginas;
        scanf("%d", &paginas);
        int  qtdPorPagina;
        printf("Digite a quantidade por pagina: ");
        scanf("%d", &qtdPorPagina);
        clock_t c1 = clock();
        cadastrarAlunosPorPaginacao(paginas, qtdPorPagina);
        clock_t c2 = clock();
        printf("\nOperacao concluida em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
    } else {
        printf("\nOpcao invalida!\n");
        menuCadastrarAlunos();
        return;
    }

    menu();
}

void menuExibirAlunos() {
    printf("\n");
    printf("1 - Ler todos os registros.\n");
    printf("2 - Por paginacao.\n");
    printf("Escolha o modo de leitura:");


    char opt[10];
    scanf("%s", opt);

    if(strcmp(opt, "1") == 0 ) {
        clock_t c1 = clock();
        lerTodosAlunos();
        clock_t c2 = clock();
        printf("\nOperacao concluida em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
    } else if(strcmp(opt, "2") == 0) {
        int qtdPorPagina;
        printf("Digite a quantidade por pagina a exibir: ");
        scanf("%d", &qtdPorPagina);

        int opc2 = 0, pagina = 1;

        while(1) {
            printf("\nPagina %d, mostrando %d registros.\n\n", pagina, qtdPorPagina);
            clock_t c1 = clock();
            mostrarPaginado(pagina, qtdPorPagina);
            clock_t c2 = clock();
            printf("\nOperacao concluida em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
            printf("\n1 - Proxima pagina\n");
            if(pagina > 1) {
                printf("2 - Pagina anterior\n");
            }
            printf("3 - Sair\n");
            printf("Digite uma das opcoes acima: ");
            scanf("%d", &opc2);
            if(opc2 == 1) {
                pagina++;
            } else if (opc2 == 2) {
                pagina--;
                if(pagina == 0) {
                    break;
                }
            } else {
                break;
            }
        }
    } else {
        printf("\nOpcao invalida!\n");
        menuExibirAlunos();
        return;
    }

    //Trazer funcoes do outro projeto

    menu();
}

void menuOrdenarAlunos() {
    printf("\n");
    printf("1 - Por matricula\n");
    printf("2 - Por nome\n");
    printf("3 - Por data nascimento\n");
    printf("\nEscolha uma das opcoes acima: ");

    char opt[10];
    scanf("%s", opt);

    if(strcmp(opt, "1") == 0) {
        ordenarAlunos(comparaAlunoPorMatricula);
    } else if(strcmp(opt, "2") == 0) {
        ordenarAlunos(comparaAlunoPorNome);
    } else if(strcmp(opt, "3") == 0) {
        ordenarAlunos(comparaAlunoPorDataNascimento);
    } else {
        printf("\nOpcao invalida!\n");
        menuOrdenarAlunos();
        return;
    }

    menu();
}

void ordenarAlunos(int(*compara)(const void*, const void*)) {
    clock_t c1 = clock();
    FILE* arquivoDesordenado = fopen(NOME_ARQUIVO, "rb");

    if (arquivoDesordenado == NULL) {
        printf("\nNao ha alunos cadastrados.\n");
        return;
    }

    double tamEmMgMemoria = 30;

    ordenarBlocos(arquivoDesordenado, sizeof(Aluno), ((1024 * 1024 * tamEmMgMemoria)/2), ordenaAlunos, compara);

    fclose(arquivoDesordenado);

    remove(NOME_ARQUIVO);

    intercalar(NOME_ARQUIVO, (1024 * 1024 * tamEmMgMemoria), sizeof(Aluno), compara);

    clock_t c2 = clock();
    printf("\nOperacao concluida em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
}

int comparaAlunoPorMatricula( const void *x, const void *y ) {
    Aluno *alunox = (Aluno*) x;
    Aluno *alunoy = (Aluno*) y;

    if( (alunox->matricula - alunoy->matricula) < 0 ){
        return -1;
    }else if ( (alunox->matricula - alunoy->matricula) > 0 ) {
        return 1;
    } else {
        return 0;
    }
}

int comparaAlunoPorNome( const void *x, const void *y ) {
    Aluno *alunox = (Aluno*) x;
    Aluno *alunoy = (Aluno*) y;

    return strcmp(alunox->nome, alunoy->nome);
}

int comparaAlunoPorDataNascimento( const void *x, const void *y ) {
    Aluno *alunox = (Aluno*) x;
    Aluno *alunoy = (Aluno*) y;

    return strcmp(alunox->dataNascimento, alunoy->dataNascimento);
}

void ordenaAlunos(void* info, int qtd, int(*compara)(const void*, const void*)) {

    Aluno* alunos = (Aluno*) info;

    if(alunos == NULL) {
        return;
    }

    qsort(alunos, qtd, sizeof(Aluno), compara);
}