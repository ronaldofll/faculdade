#include "Arvore.h"

void inicializaArvore(Arvore *arvore, int tamInfo, int capacidade) {
    arvore->tamInfo = tamInfo;
    arvore->raiz = NULL;
    arvore->capacidade = capacidade;
    arvore->qtd = 0;
}

No* criaNo(void* valor, int tamInfo) {
    //Inicializa Nó
    No* no = malloc(sizeof(No));
    no->menor = NULL;
    no->maior = NULL;

    //Copia o valor para o nó
    no->valor = malloc(tamInfo);
    memcpy(no->valor,valor,tamInfo);
    return no;
}

void insereNoEmNo(No **enderecoNo, void* valor, int tamInfo, int(*compara)(const void*, const void*)) {

    if(*enderecoNo == NULL) {
        *enderecoNo = criaNo(valor, tamInfo);
        return;
    }

    if(compara(valor,(*enderecoNo)->valor) <= 0) {
        insereNoEmNo(&(*enderecoNo)->menor, valor, tamInfo, compara);
    } else {
        insereNoEmNo(&(*enderecoNo)->maior, valor, tamInfo, compara);
    }
}

int insereNo(Arvore* arvore, void* valor, int(*compara)(const void*, const void*)) {
    if(arvore->qtd == arvore->capacidade) {
        return ERRO_CAPACIDADE_ATINGIDA;
    }

    insereNoEmNo(&arvore->raiz, valor, arvore->tamInfo, compara);
    arvore->qtd = arvore->qtd +1;
    return 0;
}

int removeMenor(No *no, void* destino, int tamInfo) {

    if(no == NULL) {
        return 0;
    }

    if(no == NULL) {
        return 0;
    }

    No* menor = NULL;

    if(no->menor == NULL) {
        menor = no;
        no = no->maior;
    } else if(no->menor->menor == NULL) {
        menor = no->menor;
        no->menor = menor->maior;
    } else {
        return removeMenor(no->menor, destino, tamInfo);
    }

    if(menor != NULL) {
        memcpy(destino, menor->valor, tamInfo);
        free(menor->valor);
        free(menor);
        return 1;
    }

    return 0;
}

int removeNo(Arvore* arvore, void* destino) {

    if(arvore->raiz == NULL) {
        return 0;
    }

    if(arvore->raiz->menor == NULL) {
        memcpy(destino, arvore->raiz->valor, arvore->tamInfo);

        No* maior = arvore->raiz->maior;

        free(arvore->raiz->valor);
        free(arvore->raiz);

        arvore->raiz = maior;
        arvore->qtd = arvore->qtd - 1;
        return 1;
    }

    int e = removeMenor(arvore->raiz, destino, arvore->tamInfo);
    if(e) {
        arvore->qtd = arvore->qtd - 1;
    }
    return e;
}

void mostraNo(No no, void(*fMostra)(void*)) {

    if(no.menor != NULL) {
        mostraNo(*no.menor, fMostra);
    }

    fMostra(no.valor);
    printf(" ");

    if(no.maior != NULL) {
        mostraNo(*no.maior, fMostra);
    }
}

void mostraArvore(Arvore arvore, void(*fMostra)(void*)) {
    if(arvore.raiz == NULL) {
        printf("\nSem dados!\n");
        return;
    }

    mostraNo(*arvore.raiz, fMostra);
}