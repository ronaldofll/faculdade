#include "Ordenacao.h"

char* dirFile(char* fileName) {
    char* dirFile = malloc(sizeof(char) * 50);

    char barra[] = "/";

    sprintf(dirFile, "%s%s%s", TEMP_DIR, barra, fileName);

    return dirFile;
}

void limparDiretorio(char* path) {
    DIR *directory;
    struct dirent* file;
    directory = opendir(path);

    while ((file=readdir(directory)) != NULL) {
        if( strcmp(file->d_name, ".") != 0 && strcmp(file->d_name, "..") != 0 ) {
            char fPath[20];
            sprintf(fPath, "%s%s%s", TEMP_DIR, "/", file->d_name);
            remove(fPath);
        }
    }
    rewinddir(directory);
}

/*Criar arquivo temporario e salva em um array*/
int salvaBlocoOrdenado(void* bloco, int tam, int qtd, void(*ordenaInfo)(void*, int, int(*comparaInfo)(const void*, const void*)), int(*compara)(const void*, const void*)) {

    static int cont = 0;

    char fileName[20];
    sprintf(fileName, "%s%d%s", TEMP_FILE, ++cont, BIN);
    char *tempFile = dirFile(fileName);

    FILE *fp = fopen(tempFile, "wb+");
    if(fp == NULL) {
        printf("\nErro na abertura do arquivo %s", tempFile);
        return 0;
    }

    ordenaInfo(bloco, qtd, compara);

    fwrite(bloco, tam, qtd, fp);

    fclose(fp);

    return 1;
}

/*Divide um arquivo inicial em varios blocos ordenados*/
int ordenarBlocos(FILE *fp, int tamInfo, int tamEmBytesDoBloco, void(*ordenaInfo)(void*, int, int(*)(const void*, const void*)), int(*compara)(const void*, const void*) ) {

    limparDiretorio(TEMP_DIR);

    fseek(fp, 0, SEEK_END);
    int qtdRegistros = ftell(fp) / tamInfo;

    fseek(fp, 0, SEEK_SET);

    int qtdPorBloco = tamEmBytesDoBloco / tamInfo;

    int qtdBlocos;
    if(qtdRegistros < qtdPorBloco) {
        qtdBlocos = 1;
        qtdPorBloco = qtdRegistros;
    } else {
        qtdBlocos = ceil(((double)qtdRegistros/qtdPorBloco));
    }

    int i;
    for(i = 0; i < qtdBlocos; i++) {

        void* info = malloc(tamInfo * qtdPorBloco);
        fread(info, tamInfo, qtdPorBloco, fp);

        salvaBlocoOrdenado(info, tamInfo, qtdPorBloco, ordenaInfo, compara);

        free(info);
    }

    return 1;
}

void salvarParaArquivoOrdenado(Arvore* arvore, int tamInfo, char* fileName) {
    int i;

    FILE* fp = fopen(fileName, "ab+");
    if(fp == NULL) {
        printf("\n Nao foi possivel criar arquivo ordenado!");
        return;
    }

    int qtd = arvore->qtd;
    void* info = malloc(qtd * tamInfo);

    for(i = 0; i < qtd; i++) {
        removeNo(arvore, info + (tamInfo * i));
    }

    fwrite(info, tamInfo, qtd, fp);

    free(info);

    fclose(fp);
}

int contarArquivos(DIR *directory) {
    int cont = 0;

    struct dirent* file;

    while ((file=readdir(directory)) != NULL) {
        if( strcmp(file->d_name, ".") != 0 && strcmp(file->d_name, "..") != 0 ) {
            cont++;
        }
    }
    rewinddir(directory);
    return cont;
}

int leERemove (char* fileName, void* info, int tamInfo, int qtd) {

    char* fileDir = dirFile(fileName);
    FILE* fp = fopen(fileDir, "r+");
    if(fp == NULL) {
        printf("\nNao foi possivel abrir arquivo temporario %s!", fileDir);
        return 0;
    }

    fseek(fp, 0, SEEK_END);
    int tam = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    if(tam == 0) {
        return 0;
    }

    int qtdNoArquivo = tam/tamInfo;
    int qtdRestante;

    if(qtdNoArquivo < qtd) {
        qtdRestante = 0;
        qtd = qtdNoArquivo;
    } else {
        qtdRestante = (tam/tamInfo)-qtd;
    }

    fread(info, tamInfo, qtd, fp);

    if(qtdRestante > 0) {
        void* infoRestantes = malloc(tamInfo * qtdRestante);
        fread(infoRestantes, tamInfo, qtdRestante, fp);

        FILE* novoArquivo = fopen(fileDir, "wb+");
        if(novoArquivo == NULL) {
            printf("\nNao foi possivel abrir arquivo %s", fileDir);
            exit(0);
        }

        fwrite(infoRestantes, tamInfo, qtdRestante, novoArquivo);
        fclose(novoArquivo);

        free(infoRestantes);
    } else {
        remove(fileDir);
    }

    fclose(fp);

    return qtd;
}

void intercalar(char* nomeArquivoDestino, long qtdMemoriaEmBytes, int tamInfo, int(*compara)(const void*, const void*)) {

    clock_t c1 = clock();
    DIR *directory;
    struct dirent* file;
    directory = opendir(TEMP_DIR);

    qtdMemoriaEmBytes = qtdMemoriaEmBytes/2;
    long qtdArquivos = contarArquivos(directory);
    long qtdMemoria = (qtdMemoriaEmBytes/2)/qtdArquivos;  //Sera utilizada uma parcela da memória total para cada arquivo.
    long qtdPorVez = qtdMemoria/tamInfo;              //Quantidade de registros a serem processados por vez pela memória.

    Arvore arvore;
    inicializaArvore(&arvore, tamInfo, ((qtdMemoriaEmBytes/2)/tamInfo));

    while(qtdArquivos > 0) {

        int i = 0, k;
        while ((file=readdir(directory)) != NULL) {

            if( strcmp(file->d_name, ".") != 0 && strcmp(file->d_name, "..") != 0 ) {

                void* bloco = malloc(tamInfo * qtdPorVez);
                int qtdLido = leERemove(file->d_name, bloco, tamInfo, qtdPorVez);

                for(k = 0; k < qtdLido; k++) {

                    void* info = bloco + (tamInfo * k);

                    int ret = insereNo(&arvore, info, compara);
                    if(ret == ERRO_CAPACIDADE_ATINGIDA) {
                        salvarParaArquivoOrdenado(&arvore, tamInfo, nomeArquivoDestino);

                        ret = insereNo(&arvore, info, compara);
                    }
                }

                free(bloco);
            }

            i++;
        }
        rewinddir(directory);
        qtdArquivos = contarArquivos(directory);

    }
    if(arvore.qtd > 0) {
        salvarParaArquivoOrdenado(&arvore, tamInfo, nomeArquivoDestino);
    }

    closedir(directory);

    clock_t c2 = clock();
    printf("\nOperacao concluida em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
}