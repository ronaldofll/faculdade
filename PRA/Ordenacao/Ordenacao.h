#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>
#include <math.h>
#include "Arvore.h"

#define TEMP_DIR "arquivos/temp"
#define TEMP_FILE "temp"
#define BIN ".bin"

//função para concatenar o nome do arquivo informado com o diretório de arquivos do programa
char* dirFile(char* fileName);

//Limpa o diretório de arquivos temporários.
void limparDiretorio(char* path);

//Salva bloco de informações já ordenados em um arquivo
int salvaBlocoOrdenado(void* bloco, int tam, int qtd, void(*ordenaInfo)(void*, int, int(*comparaInfo)(const void*, const void*)), int(*compara)(const void*, const void*));

//Ordena blocos de informações e chama a função salvaBlocoOrdenado ao concluir a ordenação
int ordenarBlocos(FILE *fp, int tamInfo, int tamEmBytesDoBloco, void(*ordenaInfo)(void*, int, int(*)(const void*, const void*)), int(*compara)(const void*, const void*) );

//Salva para o arquivo principal as informações contidas na arvore de busca
void salvarParaArquivoOrdenado(Arvore* arvore, int tamInfo, char* fileName);

//Conta quantidade de arquivos no diretório
int contarArquivos(DIR *directory);

//Faz a leitura de um arquivo e copia suas informações para o ponteiro passado no parâmetro
int leERemove (char* fileName, void* info, int tamInfo, int qtd);

//Utiliza os arquivos temporários para intercalar e gerar o 
void intercalar(char* nomeArquivoDestino, long qtdMemoriaEmBytes, int tamInfo, int(*compara)(const void*, const void*));