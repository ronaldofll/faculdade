#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ERRO_CAPACIDADE_ATINGIDA -1

typedef struct N {
    void *valor;
    struct N *menor;
    struct N *maior;
} No;

typedef struct A {
    No* raiz;
    int tamInfo;
    int capacidade;
    int qtd;
} Arvore;

//Inicializ a arvore
void inicializaArvore(Arvore *arvore, int tamInfo, int capacidade);

//Cria e retorna um elemento do tipo Nó com o valor informado.
No* criaNo(void* valor, int tamInfo);

//Insere um valor na arvore
int insereNo(Arvore* arvore, void* valor, int(*compara)(const void*, const void*));

//Remove um valor da arvore e copia sua informação no parâmetro destino
int removeNo(Arvore* arvore, void* destino);

//Mostra o valor no Nó informado.
void mostraNo(No no, void(*fMostra)(void*));

//Mostra todos os valores da arvore utilizando o método de busca em ordem.
void mostraArvore(Arvore arvore, void(*fMostra)(void*));