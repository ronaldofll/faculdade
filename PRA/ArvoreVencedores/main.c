#include <stdio.h>
#include <stdlib.h>
#include "Arvore.h"

void mostraInt(void* x) {
    int *valor = (int*) x;
    printf("%i", *valor);
}

int comparaInt(const void *x, const void *y) {
    int* a = (int*) x;
    int* b = (int*) y;

    return (*a) - (*b);
}

int main(){

    Arvore arvore;
    inicializaArvore(&arvore, sizeof(int), 10);

    int x = 3;
    insereNo(&arvore, &x, comparaInt);

    x=1;
    insereNo(&arvore, &x, comparaInt);

    x=4;
    insereNo(&arvore, &x, comparaInt);

    x=2;
    insereNo(&arvore, &x, comparaInt);

    x=10;
    insereNo(&arvore, &x, comparaInt);

    x=1;
    insereNo(&arvore, &x, comparaInt);

    x=2;
    insereNo(&arvore, &x, comparaInt);

    x=20;
    insereNo(&arvore, &x, comparaInt);

    x=0;
    insereNo(&arvore, &x, comparaInt);

    printf("\n\n");
    while(arvore.qtd > 0 ) {
        removeNo(&arvore, &x);
        printf("%d ", x);
        getchar();
    }

    mostraArvore(arvore, mostraInt);

    return 0;
}