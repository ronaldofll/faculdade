#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pilha.h"

void inicializa_pilha(Pilha *p, int c, int t)
{
    p->dados = malloc (sizeof(void *)*c);
    p->capacidade = c;
    p->topo = -1;
    p->tamInfo = t;
}

int empilha(Pilha *p, void *info)
{
    if(pilha_cheia(*p))
        return ERRO_PILHA_CHEIA;

    p->topo ++;
    p->dados[p->topo] = malloc(p->tamInfo);
    memcpy(p->dados[p->topo], info, p->tamInfo);
    return 1;
}

int pilha_vazia(Pilha p)
{
    if (p.topo == -1)
        return 1;
    else
        return 0;
}

int pilha_cheia(Pilha p)
{
    return p.topo == p.capacidade - 1;
}

int desempilha(Pilha *p, char *info)
{
    if(pilha_vazia(*p))
        return ERRO_PILHA_VAZIA;

    memcpy(info, p->dados[p->topo], p->tamInfo);
    free(p->dados[p->topo]);

    p->topo--;

    return 1;
}

void desaloca_pilha(Pilha *p)
{
    free(p->dados);
}
