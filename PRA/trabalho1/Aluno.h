#include <stdio.h>

#define NOME_ARQUIVO "arquivos/arquivo.bin"
#define NOMES_ARQUIVO "arquivos/nomes.bin"
#define SOBRENOMES_ARQUIVO "arquivos/sobrenomes.bin"
#define QTD_DISCIPLINAS 30

typedef struct a
{
    long matricula;
    char nome[32];
    char dataNascimento[11];
    char disciplinas[10][60];
    int qtdDisciplinas;
} Aluno;

Aluno gerarAluno();

void lerPosicao(FILE *fp, long pos);

void lerTodosAlunos();

void lerPaginado(int pg, int qtd);

void mostrarAluno(Aluno aluno);

void cadastrarAlunosPorTamanhoArquivo(long tamEmKbs);

void cadastrarAlunosPorPaginacao(int paginas, int quantidadePorPagina);

void limparRegistroAlunos();