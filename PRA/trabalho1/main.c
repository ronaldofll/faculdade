//alias tra1="cd /home/ronaldo/workspace/git/faculdade/PRA/trabalho1 && gcc main.c Aluno.c Random.c -o main.e && ./main.e && mv arquivos/arquivo.bin ../Ordenacao/arquivos/arquivo.bin"

#include <stdlib.h>
#include <string.h>
#include <stdlib.h> 
#include <time.h>
#include "Random.h"
#include "Aluno.h"

#define NOME_ARQUIVO "arquivos/arquivo.bin"

void menu();
void cadastrarAlunos();
void lerAlunos();
void limparRegistros();

int main()
{
    srand(time(NULL));

    //cadastrarAlunosPorTamanhoArquivo(1000*1001);
    //cadastrarAlunosPorPaginacao(1, 2);

    //printf("\n\n");
    //lerTodosAlunos();
    //lerPaginado(1, 2);

    menu();

    return 0;
}

void menu(){
    printf("\n\n1 - Cadastrar alunos\n");
    printf("2 - Ler alunos\n");
    printf("3 - Limpar registros\n");
    printf("4 - Sair\n");
    printf("Escolha uma das opções acima: ");

    int opc;
    scanf("%i", &opc);

    switch(opc){
        case 1:
            cadastrarAlunos();
            break;
        case 2: 
            lerAlunos();
            break;
        case 3: 
            limparRegistros();
            break;
        case 4:
            break;
        default:
            printf("\nOpção inválida!\n");
            menu();
            break;
    }
}

void cadastrarAlunos() {

    printf("\n\n1 - Por tamanho do arquivo\n");
    printf("2 - Por paginação\n");
    printf("Escolha o modo de cadastramento:");

    int opc;
    scanf("%i", &opc);

    switch(opc){
        case 1: {
            printf("\nDigite o tamanho do arquivo em kbs: ");
            long tamArquivo;
            scanf("%li", &tamArquivo);
            clock_t c1 = clock();            
            cadastrarAlunosPorTamanhoArquivo(tamArquivo);
            clock_t c2 = clock();            
            printf("\nOperação concluída em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
            break;
        }
        case 2: {
            printf("\nDigite a quantidade de páginas: ");
            int paginas;
            scanf("%d", &paginas);
            int  qtdPorPagina;
            printf("Digite a quantidade por página: ");
            scanf("%d", &qtdPorPagina);
            clock_t c1 = clock();            
            cadastrarAlunosPorPaginacao(paginas, qtdPorPagina);
            clock_t c2 = clock();            
            printf("\nOperação concluída em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
            break;
        }
        default: {
            printf("\nOpção inválida\n");
            cadastrarAlunos();
            break;
        }
    }
    menu();
}

void lerAlunos() {
    printf("\n\n1 - Ler todos os registros.\n");
    printf("2 - Por paginação.\n");
    printf("Escolha o modo de leitura:");

    int opc;
    scanf("%i", &opc);

    switch(opc) {
        case 1: {
            clock_t c1 = clock();            
            lerTodosAlunos();
            clock_t c2 = clock();            
            printf("\nOperação concluída em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
            break;
        }
        case 2: {
            int qtdPorPagina;
            printf("Digite a quantidade por página à exibir: ");
            scanf("%d", &qtdPorPagina);

            int opc2 = 0, pagina = 1;

            while(1) {
                printf("\nPagina %d, mostrando %d registros.\n\n", pagina, qtdPorPagina); 
                clock_t c1 = clock();            
                lerPaginado(pagina, qtdPorPagina);
                clock_t c2 = clock();            
                printf("\nOperação concluída em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
                printf("\n1 - Próxima página\n");
                if(pagina > 1) {
                    printf("2 - Pagina anterior\n");
                }
                printf("3 - Sair\n");
                printf("Digite uma das opções acima: ");
                scanf("%d", &opc2);
                if(opc2 == 1) {
                    pagina++;
                } else if (opc2 == 2) {
                    pagina--;
                    if(pagina == 0) {
                        break;
                    }
                } else {
                    break;
                }           
            }

            break;
        }
        default: {
            printf("\nOpção inválida\n");
            break;
        }
    }

    menu();
}

void limparRegistros() {
    clock_t c1 = clock();            
    limparRegistroAlunos();
    clock_t c2 = clock();            
    printf("\nOperação concluída em %.4f segundos\n", (c2-c1) / (double)CLOCKS_PER_SEC);
    menu();
}