#include "Aluno.h"
#include "Random.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int ultMatricula = 0;

char disciplinas[QTD_DISCIPLINAS][10] = {"AGT0001","ALGA001","CDI0001","MCI0001","TGA0002","AOC0002","EST0006","LPG0002","MAT0002","TGS0002","ANA1001","EDA0001","FSI0002","GFC0001","POO0001","ANA2001","BAN1001","PRA0001","SOFT002","SOP0002","BAN2001","ECS1004","EMI0002","PES0002","REC0002","ATC0009","DIR0002","ECS2004","ETI0002","GPR0002"};

void lerNomeAleatorio(FILE *f, char *destino) {
    char linha[30];

    fseek(f, 0, SEEK_END);
    int qtdRegistros = ftell(f) / sizeof(linha);

    int xLinha = randomNumber(1, qtdRegistros);

    fseek(f, xLinha * sizeof(linha), SEEK_SET);
    fread(linha, sizeof(linha), 1, f);

    strcpy(destino, linha);

    fseek(f, 0, SEEK_SET);
}

void lerPosicao(FILE *fp, long pos) {

    fseek(fp, 0, SEEK_END);
    long qtdTotal = ftell(fp)/sizeof(Aluno);

    if(pos >= qtdTotal) {
        return;
    }

    Aluno aluno;
    fseek(fp, pos*(sizeof(Aluno)), SEEK_SET);
    fread(&aluno, 1, sizeof(Aluno), fp);
    mostrarAluno(aluno);
}

void lerTodosAlunos() {

    FILE *fp = fopen(NOME_ARQUIVO, "rb");
    if(fp == NULL) {
        printf("\n\nNão foi possível abrir o arquivo!\n");
        return;
    }

    fseek(fp, 0, SEEK_END);
    int qtd = ftell(fp) / sizeof(Aluno);

    if(qtd == 0) {
        printf("\nNenhum registro\n");
        return;
    }

    printf("\n%-10s|%-32s|%-50s|\n", "MATRICULA", "NOME", "DISCIPLINAS");
    printf("%s-%s-%s-\n", "----------", "--------------------------------", "--------------------------------------------------");

    int i = 0;

    fseek(fp, 0, SEEK_SET);
    while(ftell(fp) < qtd * sizeof(Aluno)) {
        Aluno aluno;
        fread(&aluno, 1, sizeof(Aluno), fp);
        mostrarAluno(aluno);
    }
    printf("%s-%s-%s-\n", "----------", "--------------------------------", "--------------------------------------------------");

    fclose(fp);
}

void lerPaginado(int pg, int qtd) {

    FILE *fp = fopen(NOME_ARQUIVO, "rb");
    if(fp == NULL) {
        printf("\n\nNão foi possível abrir o arquivo!\n");
        return;
    }

    if(qtd == 0) {
        return;
    }

    fseek(fp, 0, SEEK_END);
    int qtdTotal = ftell(fp) / sizeof(Aluno);

    if(qtdTotal == 0) {
        printf("\nNenhum registro\n");
        return;
    }

    int inicio = (pg-1)*qtd;

    printf("%-10s|%-32s|%-50s|\n", "MATRICULA", "NOME", "DISCIPLINAS");

    if((inicio + qtd) > qtdTotal) {
        return;
    }

    fseek(fp, inicio * sizeof(Aluno), SEEK_SET);

    Aluno *alunos = malloc(sizeof(Aluno) * qtd);
    fread(alunos, qtd, sizeof(Aluno), fp);

    int i;
    for(i = 0; i < qtd; i++) {
        mostrarAluno(alunos[i]);
    }

    fclose(fp);
}

void mostrarAluno(Aluno aluno) {
    char matricula[10], disciplinas[50] = "";
    sprintf(matricula, "%09li", aluno.matricula);
    int i;
    for(i=0; i < aluno.qtdDisciplinas; i++) {
        if(i > 0) {
            strcat(disciplinas, ",");
        }
        strcat(disciplinas, aluno.disciplinas[i]);
    }


    printf("%-10s|%-32s|%-50s|", matricula, aluno.nome, disciplinas);
    printf("\n");
}

Aluno gerarAluno(FILE *fNomes, FILE *fSobrenomes) {

    //MATRICULA
    long matricula = ++ultMatricula;

    //DATA NASCIMENTO
    char dataNasc[11];
    int dia = randomNumber(1, 30);
    int mes = randomNumber(1, 12);
    int ano = randomNumber(1920, 2010);

    sprintf(dataNasc, "%04d/%02d/%02d", ano, mes, dia);

    //NOME
    char nome[20];
    lerNomeAleatorio(fNomes, nome);

    //SOBRENOME
    char sobrenome[20];
    lerNomeAleatorio(fSobrenomes, sobrenome);

    //NOME e SOBRENOME
    char nomeSobrenome[42];
    sprintf(nomeSobrenome, "%s %s", nome, sobrenome);

    //Disciplinas
    int codDisciplinas[QTD_DISCIPLINAS];
    int qtdDisciplinas = randomNumber(1, 5);
    int cont = 0;

    while (cont < qtdDisciplinas)
    {
        int x = randomNumber(1, QTD_DISCIPLINAS);

        int jaExiste = 0;
        int i;
        for (i = 0; i < QTD_DISCIPLINAS; i++)
        {
            if (codDisciplinas[cont] == x)
            {
                jaExiste = 1;
                break;
            }
        }

        if (!jaExiste)
        {
            codDisciplinas[cont++] = x-1;
        }
    }

    Aluno aluno;
    aluno.matricula = matricula;
    strcpy(aluno.dataNascimento, dataNasc);
    strcpy(aluno.nome, nomeSobrenome);
    int i;
	for (i = 0; i < qtdDisciplinas; i++){
		strcpy(aluno.disciplinas[i], disciplinas[codDisciplinas[i]]);
    }
    aluno.qtdDisciplinas = qtdDisciplinas;
    return aluno;
}

void gravarAluno(FILE *fp, Aluno *aluno) {
    fwrite(aluno, sizeof(Aluno), 1, fp);
}

void gravarAlunos(FILE *fp, Aluno *alunos, int qtd) {
    fwrite(alunos, sizeof(Aluno), qtd, fp);
}

void cadastrarAlunosPorTamanhoArquivo(long tamEmKbs) {

    FILE *fp = fopen(NOME_ARQUIVO, "w");
    FILE *fNomes = fopen(NOMES_ARQUIVO, "rb");
    FILE *fSobrenomes = fopen(SOBRENOMES_ARQUIVO, "rb");

    if(fp == NULL || fNomes == NULL || fSobrenomes == NULL) {
        printf("\n\nErro ao abrir arquivo!\n");
    }

    long tamanho = 0;

    while ((tamanho + sizeof(Aluno)) <= (tamEmKbs * 1000)) {
        Aluno aluno = gerarAluno(fNomes, fSobrenomes);
        //mostrarAluno(aluno);
        gravarAluno(fp, &aluno);
        tamanho = ftell(fp);
        //printf("%li kbs\n", tamanho/1024);
    }

    fclose(fp);
    fclose(fNomes);
    fclose(fSobrenomes);
}

void cadastrarAlunosPorPaginacao(int paginas, int quantidadePorPagina) {

    FILE *fp = fopen(NOME_ARQUIVO, "w");
    if(fp == NULL) {
        printf("\n\nNão foi possível abrir arquivo para gravação!\n");
        return;
    }


    if(paginas <= 0 || quantidadePorPagina <=0 ) {
        return;
    }

    FILE *fNomes = fopen(NOMES_ARQUIVO, "rb");
    FILE *fSobrenomes = fopen(SOBRENOMES_ARQUIVO, "rb");

    int qtd = paginas * quantidadePorPagina;

    Aluno *alunos = malloc(sizeof(Aluno) * qtd);

    int i;
    for(i = 0; i < qtd; i++) {
        Aluno aluno = gerarAluno(fNomes, fSobrenomes);
        memcpy(&alunos[i], &aluno, sizeof(Aluno));
        //mostrarAluno(alunos[i]);
    }

    gravarAlunos(fp, alunos, qtd);

    fclose(fp);
    fclose(fNomes);
    fclose(fSobrenomes);
}

void limparRegistroAlunos() {
    FILE *fp;
    fp = fopen(NOME_ARQUIVO, "wb");
    if(fp != NULL) {
        printf("\nOs registros foram apagados!\n");
    } else {
        printf("\nOcorreu um erro ao apagar registros!\n");
    }
}