#include <stdio.h>
#include <stdlib.h>
#include "Fila.h"

void inicializaFila(Fila* f, int capacidade) {
	f->dados = malloc(sizeof(int)*capacidade);
	f->capacidade = capacidade;
	f->inicio = 0;
	f->fim = 0;	
	f->quantidade = 0;
}

int inserir(Fila* f, int x) {
	if(filaCheia(*f)){
		printf("\nFila cheia\n");
		return ERRO_FILA_CHEIA;
	}
	f->dados[f->fim] = x;
	f->quantidade++;
	if(f->fim == f->capacidade-1){
		f->fim = 0;
	}else{
		f->fim++;
	}
	return 1;
}

void mostra(Fila f) {
	if(filaVazia(f)){
		printf("\nFila vazia\n");
	}else{

		int i = f.inicio;
		do{
			printf("%i\n", f.dados[i]);

			if(i == f.capacidade-1){
				i = 0;
			}else{
				i++;
			}
		}while(i != f.fim);
	}
}

int remover(Fila* f, int* x) {
	if(filaVazia(*f)){
		printf("\nFila vazia\n");
		return ERRO_FILA_VAZIA;
	}
	*x = f->dados[f->inicio];
	f->quantidade--;
	if(f->inicio == f->capacidade-1){
		f->inicio = 0;
	}
	else{
		f->inicio++;
	}
	return 1;
}

void desaloca(Fila* f) {
	free(f->dados);
}

int filaCheia(Fila f){
	printf("\n\t %i, %i\n", f.capacidade, f.quantidade);
	if(f.capacidade == f.quantidade){
		return 1;
	}
	return 0;
}
int filaVazia(Fila f){
	if(f.quantidade == 0){
		return 1;
	}
	return 0;
}
