#include <stdio.h>
#include "Fila.h"

void menu();
void inserirNaFila();
void removerDaFila();
void mostrarFila();
float lerFloat();
int lerInt();
void mostraFloat(void* x) ;
void mostraInt(void* x) ;

Fila fila;

int main(){

	inicializaFila(&fila, 3);
	menu();

	desaloca(&fila);
}


void menu() {
	int opc;

	printf("\n...:::MENU:::....\n");

	printf("1.Inserir\n");
	printf("2.Remover\n");
	printf("3.Mostrar\n");
	printf("4.Sair\n");
	printf("\nOpção: ");
	scanf("%d", &opc);

	switch(opc){
		case 1:
			inserirNaFila();
			menu();
			break;
		case 2:
			removerDaFila();
			menu();
			break;
		case 3:
			mostrarFila();
			menu();
			break;
		case 4:
			break;
		default:
			printf("\nOpção inválida!\n");
			menu();
	}
}

void inserirNaFila(){
	printf("Digite um número: ");
	int numero = lerInt();
	inserir(&fila, numero);
}

void removerDaFila(){
	int numero;
	remover(&fila, &numero);
	printf("\nValor Removido: %i\n", numero);
}

void mostrarFila(){
	mostra(fila);
}

float lerFloat(){
	float x;
	scanf("%f", &x);
	return x;
}

int lerInt(){
	int x;
	scanf("%d", &x);
	return x;	
}

void mostraFloat(void* x) {
	float* f =(float*)  x;
	printf("%.2f\n", *f);
}

void mostraInt(void* x) {
	int* i = (int*) x;
	printf("%i\n", *i);
}