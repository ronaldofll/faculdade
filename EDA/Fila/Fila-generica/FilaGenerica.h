#define ERRO_PILHA_CHEIA -1;
#define ERRO_PILHA_VAZIA -2;

typedef struct {
	void** dados;
	int inicio;
	int fim;
	int capacidade;
	int qtd;
	int tamInfo;
} FilaGenerica;

void inicializaFila(FilaGenerica* f, int capacidade, int tamInfo);

int inserir(FilaGenerica* f, void* info);

int remover(FilaGenerica* f, void* destino);

void mostra(FilaGenerica f, void(*mostra)(void*));

void desaloca(FilaGenerica* f);

int filaCheia(FilaGenerica f);

int filaVazia(FilaGenerica f);
