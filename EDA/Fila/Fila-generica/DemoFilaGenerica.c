#include <stdio.h>
#include "FilaGenerica.h"


void mostraFloat(void* x);


int main(){

	FilaGenerica fila;

	inicializaFila(&fila, 5, 4);

	float x = 10.0;
	inserir(&fila, &x);

	x = 15.0;
	inserir(&fila, &x);

	mostra(fila, mostraFloat);

	float y = 0;
	remover(&fila, &y);

	mostra(fila, mostraFloat);

	desaloca;


}

void mostraFloat(void* x) {
	float* f = x;
	printf("%.2f\n", *f);
}