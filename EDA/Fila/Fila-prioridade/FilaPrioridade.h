#define ERRO_PILHA_CHEIA -1;
#define ERRO_PILHA_VAZIA -2;

typedef struct {
	void** dados;
	int capacidade;
	int qtd;
	int tamInfo;
} FilaPrioridade;


void inicializaFila(FilaPrioridade* f, int capacidade, int tamInfo);

int inserir(FilaPrioridade* f, void* info, int(*compara)(void*,void*));

int remover(FilaPrioridade* f, void* destino);

void mostra(FilaPrioridade f, void(*mostra)(void*));

void desaloca(FilaPrioridade* f);

int filaCheia(FilaPrioridade f);

int filaVazia(FilaPrioridade f);
