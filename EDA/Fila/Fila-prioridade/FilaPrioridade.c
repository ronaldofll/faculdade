#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "FilaPrioridade.h"


void inicializaFila(FilaPrioridade* f, int capacidade, int tamInfo) {
	f->dados = malloc(tamInfo * capacidade);
	
	f->capacidade = capacidade;
	f->tamInfo = tamInfo;
	f->qtd = 0;
}

int inserir(FilaPrioridade* f, void* info, int(*compara)(void*,void*)) {
	if(filaCheia(*f)){
		return ERRO_PILHA_CHEIA;
	}
	int i=0,j=0;
	while(i < f->qtd && compara(f->dados[i], info) > 0){
		i++;
	}
	for(j=f->qtd; j>i; j--){
		f->dados[j]= f->dados[j-1];
	}
	f->qtd++;
	f->dados[i] = malloc(f->tamInfo);
	memcpy(f->dados[i], info, f->tamInfo);

	return 1;
}

int remover(FilaPrioridade* f, void* destino) {
	if(filaVazia(*f)){
		return ERRO_PILHA_VAZIA;
	}else{
		memcpy(destino, f->dados[f->qtd-1], f->tamInfo);
		free(f->dados[f->qtd-1]);
		f->qtd--;
	}

	return 1;
}

void mostra(FilaPrioridade f, void(*mostra)(void*)) {
	if(filaVazia(f)){
		printf("\nFila vazia!\n");
	}else{
		printf("\nFila Prioridade:\n");
		int i;
		for(i=0; i < f.qtd; i++){
			printf("Fila[%i] = ", i);
			mostra(f.dados[i]);
		}
	}
}

void desaloca(FilaPrioridade* f) {
	int i;
	for(i = 0; i < f->capacidade-1; i++){
		free(f->dados[i]);
	}

	free(f->dados);
}

int filaCheia(FilaPrioridade f) {
	if(f.qtd == f.capacidade){
		return 1;
	}else {
		return 0;
	}
}

int filaVazia(FilaPrioridade f) {
	if(f.qtd == 0) {
		return 1;
	}else{
		return 0;
	}
}
