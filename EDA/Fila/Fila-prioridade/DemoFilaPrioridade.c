#include <stdio.h>
#include "FilaPrioridade.h"

int comparaFloat(void* x,void* y);
void mostraFloat(void* x) ;


int main(){

	FilaPrioridade f;

	inicializaFila(&f, 3, 4);

	float x = 10.0;
	inserir(&f, &x, comparaFloat);

	x = 15.0;
	inserir(&f, &x, comparaFloat);

	mostra(f, mostraFloat);

	float y = 0;
	remover(&f, &y);


	x = 7.0;
	inserir(&f, &x, comparaFloat);
	remover(&f, &y);

	printf("\ny= %.2f\n", y);

	mostra(f, mostraFloat);

	desaloca(&f);

	return 1;
}

int comparaFloat(void* x,void* y){
	float *a=x, *b=y;
	if(*a==*b){
		return 0;
	}
	else if(*a < *b){
		return -1;
	}else{
		return 1;
	}
}

void mostraFloat(void* x) {
	float* f = x;
	printf("%.2f\n", *f);
}