#include <stdlib.h>
#include <stdio.h>
#include "Pilha.h"
#define ERRO_PILHA_CHEIA -1
#define ERRO_PILHA_VAZIA -2

void inicializaPilha(Pilha* pilha, int capacidade){
	int* dadosDaPilha = (int*) malloc(sizeof(int) * capacidade);
	pilha->dados = dadosDaPilha;
	pilha->capacidade = capacidade;
	pilha->topo = -1;
}

int empilha(Pilha* pilha, int valor){
	if(pilhaCheia(*pilha)){
		return ERRO_PILHA_CHEIA;	
	}

	pilha->dados[++pilha->topo] = valor;
	return 0;
}

int desempilha(Pilha* pilha, int* destino){
	if(pilhaVazia(*pilha)){
		return ERRO_PILHA_VAZIA;
	}

	*destino = pilha->dados[pilha->topo--];
	return 0;
}

void mostraPilha(Pilha pilha){
	if(pilhaVazia(pilha)){
		printf("\nPilha vazia!\n");
	}else{
		int i;
		printf("\n\tDados da Pilha:\n");
		for(i = 0; i <= pilha.topo; i++){
			printf("\tPilha[%i] = %i\n", i, pilha.dados[i]);
		}
		printf("\n");
	}
}

void desalocaPilha(Pilha* pilha){
	free(pilha->dados);
}

int pilhaVazia(Pilha pilha) {
	if(pilha.topo == -1) {
		return 1;
	}else{
		return 0;
	}
}

int pilhaCheia(Pilha pilha) {
	if(pilha.topo == pilha.capacidade-1){
		return 1;
	}else {
		return 0;
	}
}