
typedef struct {
	int* dados;
	int topo;
	int capacidade;
} Pilha;


void inicializaPilha(Pilha* pilha, int capacidade);

int empilha(Pilha* pilha, int valor);

int desempilha(Pilha* pilha, int* destino);

void mostraPilha(Pilha pilha);

void desalocaPilha(Pilha* pilha);

int pilhaVazia(Pilha pilha);

int pilhaCheia(Pilha pilha);
