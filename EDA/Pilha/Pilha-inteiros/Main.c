#include<stdio.h>
#include"Pilha.h"

int main() {
	Pilha p1;

	inicializaPilha(&p1, 50); //50 = capacidade
	mostraPilha(p1);
	empilha(&p1, 3); 		  // 3 = valor inserido na pilha
	
	int x = 10;
	empilha(&p1, x);          // Empilha o valor 10 na pilha

	mostraPilha(p1);

	int y;
	desempilha(&p1, &y);      //Variável que recebe o valor desempilhado.
	printf("Valor desempilhado: %i\n", y);

	mostraPilha(p1);
	desalocaPilha(&p1);

	return 0;
}