#include <stdlib.h>
#include <stdio.h>
#include <string.h> //mencpy(destino, origem, tamanho);
#include "PilhaGenerica.h"
#define ERRO_PILHA_CHEIA -1
#define ERRO_PILHA_VAZIA -2

void inicializaPilha(PilhaGenerica* pilha, int capacidade, int tamInfo){
	void** dadosDaPilhaGenerica = (void**) malloc(sizeof(void*) * capacidade);
	pilha->dados = dadosDaPilhaGenerica;
	pilha->capacidade = capacidade;
	pilha->topo = -1;
	pilha->tamInfo = tamInfo;

}

int empilha(PilhaGenerica* pilha, void* info){
	if(pilhaCheia(*pilha)){
		return ERRO_PILHA_CHEIA;	
	}
	pilha->topo++;
	pilha->dados[pilha->topo] = malloc(pilha->tamInfo);
	memcpy(pilha->dados[pilha->topo], info, pilha->tamInfo);
	return 0;
}

int desempilha(PilhaGenerica* pilha, void* destino){
	if(pilhaVazia(*pilha)){
		return ERRO_PILHA_VAZIA;
	}

	memcpy(destino, pilha->dados[pilha->topo], pilha->tamInfo);
	free(pilha->dados[pilha->topo]);
	pilha->topo--;
	return 1;
}

void mostraPilhaGenerica(PilhaGenerica pilha, void (*mostra)(void*) ) { //A função que exibe o dado é passada como parâmetro
	printf("\n   ##############################\n");
	if(pilhaVazia(pilha)){
		printf("\n\tPilhaGenerica vazia!\n");
		printf("\n   ##############################\n\n");
	}else{
		printf("\n");
		int i;
		for(i = 0; i <= pilha.topo; i++){
			printf("\tDados[%i]: ", i+1);
			mostra(pilha.dados[i]); //A biblioteca utiliza a função passada como parâmetro para imprimir o dado.
			printf("\n");
		}
		printf("\n   ##############################\n\n");
	}
}

void desalocaPilhaGenerica(PilhaGenerica* pilha){
	int i;
	for(i=0; i < pilha->topo; i++){
		free(pilha->dados[i]);
	}
	free(pilha->dados);
}

int pilhaVazia(PilhaGenerica pilha) {
	if(pilha.topo == -1) {
		return 1;
	}else{
		return 0;
	}
}

int pilhaCheia(PilhaGenerica pilha) {
	if(pilha.topo == pilha.capacidade-1){
		return 1;
	}else {
		return 0;
	}
}