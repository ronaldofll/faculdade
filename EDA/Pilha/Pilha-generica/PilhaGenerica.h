
typedef struct {
	void** dados;
	int topo;
	int capacidade;
	int tamInfo;
} PilhaGenerica;


void inicializaPilha(PilhaGenerica* pilha, int capacidade, int tamInfo); // inicializaPilha(&pilha, capacidade, sizeof(float))

int empilha(PilhaGenerica* pilha, void* valor);

int desempilha(PilhaGenerica* pilha, void* destino);

void mostraPilhaGenerica(PilhaGenerica pilha, void (*mostra)(void*));

void desalocaPilhaGenerica(PilhaGenerica* pilha);

int pilhaVazia(PilhaGenerica pilha);

int pilhaCheia(PilhaGenerica pilha);
