#include "PilhaGenerica.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int eh_palindrome(char* s);
//fila dupla
//fila de prioridades
//palindrome, main, sem implementacao de funcoes
int main () {

	char* s = "ARARA";

	if(eh_palindrome(s) == 1){
		printf("\nÉ palindrome!\n");
	}else {
		printf("\nNão é palindrome!\n");
	}

	return 1;
}

int eh_palindrome(char* s){
	PilhaGenerica p;
	int n = strlen(s);

	inicializaPilhaGenerica(&p, n, sizeof(char));
	int i = 0;
	for(i = 0; i < n; i++){
		empilha(&p, &s[i]);
	}

	char *temp = malloc(sizeof(char)*(n+1));
	temp[n] = '\0';

	for(i = 0; i < n; i++){
		desempilha(&p, &temp[i]);
	}

	n = strcmp(s, temp); //Compara duas strings, se o resultado = 0 então são iguais.

	desalocaPilhaGenerica(&p);
	free(temp);

	return n == 0;

}

int eh_palindrome2(char* s){
	int n = strlen(s);

	char* p1 = s;
	char* p2 = s + n-1;

	while(p1 < p2) {
		if( *p1 != *p2) {
			return 0;
		}
		p1++;
		p2--;
	}
	return 1;
}