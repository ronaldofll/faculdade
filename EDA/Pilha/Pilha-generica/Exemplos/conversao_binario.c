#include<stdio.h>
#include"PilhaGenerica.h"

void mostraInt(void* dado);
void mostraFloat(void* dado);

int main() {

	PilhaGenerica pilha;

	inicializaPilhaGenerica(&pilha, 8, sizeof(int));

	int x; //1010
	scanf("%i", &x);
	printf("(%i): (", x);

	int i = 0, resto, y;
	while(x > 0) {
		resto = x % 2;
		empilha(&pilha, &resto);
		x = x / 2;
	}

	while(!pilhaVazia(pilha)) {
		desempilha(&pilha, &y);
		printf("%i", y);
	}
	printf(")\n");

	desalocaPilhaGenerica(&pilha);

	return 0;
}

void mostraFloat(void* dado) {
	float* f = dado;
	printf("%.2f", *f);
}

void mostraInt(void* dado) {
	int* i = dado;
	printf("%i", *i);
}