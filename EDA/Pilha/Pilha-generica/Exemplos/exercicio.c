#include<stdio.h>
#include"PilhaGenerica.h"

void mostraInt(void* dado);
void mostraFloat(void* dado);

int main() {

	PilhaGenerica pilha;

	inicializaPilhaGenerica(&pilha, 8, sizeof(int));

	int x = 11;
	printf("(%i): (", x);

	int i = 0, resultado = 0;
	do{
		resultado = x / 2;
		printf("%i", resultado);
		int resto = x % 2;

		x = resultado;
		empilha(&pilha, &resto);
	}while(resultado != 0);

	for(i = 0; i < pilha.topo; i++) {
		int y = 0;
		desempilha(&pilha, &y);
		printf("%i", y);
	}
	printf(")\n");

	return 0;
}

void mostraFloat(void* dado) {
	float* f = dado;
	printf("%.2f", *f);
}

void mostraInt(void* dado) {
	int* i = dado;
	printf("%i", *i);
}