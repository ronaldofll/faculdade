#include<stdio.h>
#include"PilhaGenerica.h"

void mostraFloat(void* dado);

int main() {
	PilhaGenerica p1;

	inicializaPilha(&p1, 50, sizeof(float)); //50 = capacidade
	mostraPilhaGenerica(p1, mostraFloat);
	
	float x = 3;
	empilha(&p1, &x);          // Empilha o valor 10 na pilha
	x=2;
	empilha(&p1, &x); 
	float y;
	desempilha(&p1, &y);
	printf("Valor removido: %.2f\n", y);

	mostraPilhaGenerica(p1, mostraFloat);

	desempilha(&p1, &y);      //Variável que recebe o valor desempilhado.
	printf("Valor desempilhado: %.2f\n", y);

	mostraPilhaGenerica(p1, mostraFloat);
	desalocaPilhaGenerica(&p1);

	return 0;
}

void mostraFloat(void* dado) {
	float* f = dado;
	printf("%.2f", *f);
}