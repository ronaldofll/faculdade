#include <stdlib.h>
#include <stdio.h>
#include <string.h> //mencpy(destino, origem, tamanho);
#include "PilhaDupla.h"

void inicializaPilha(Pilha* pilha, int capacidade, int tamInfo){
	void** dadosDaPilha = (void**) malloc(sizeof(void*) * capacidade);
	pilha->dados = dadosDaPilha;
	pilha->capacidade = capacidade;
	pilha->topo1 = -1;
	pilha->topo2 = capacidade;
	pilha->tamInfo = tamInfo;

}

int empilha1(Pilha* pilha, void* info){
	if(pilhaCheia(*pilha)){
		return ERRO_PILHA_CHEIA;	
	}
	pilha->topo1++;
	pilha->dados[pilha->topo1] = malloc(pilha->tamInfo);
	memcpy(pilha->dados[pilha->topo1], info, pilha->tamInfo);
	return 0;
}

int empilha2(Pilha* pilha, void* info){
	if(pilhaCheia(*pilha)){
		return ERRO_PILHA_CHEIA;	
	}
	pilha->topo2--;
	pilha->dados[pilha->topo2] = malloc(pilha->tamInfo);
	memcpy(pilha->dados[pilha->topo2], info, pilha->tamInfo);
	return 0;
}

int desempilha1(Pilha* pilha, void* destino){
	if(pilhaVazia1(*pilha)){
		return ERRO_PILHA_VAZIA;
	}

	memcpy(destino, pilha->dados[pilha->topo1], pilha->tamInfo);
	free(pilha->dados[pilha->topo1]);
	pilha->topo1--;
	return 1;
}

int desempilha2(Pilha* pilha, void* destino){
	if(pilhaVazia2(*pilha)){
		return ERRO_PILHA_VAZIA;
	}

	memcpy(destino, pilha->dados[pilha->topo2], pilha->tamInfo);
	free(pilha->dados[pilha->topo2]);
	pilha->topo2++;
	return 1;
}

void mostraPilha1(Pilha pilha, void (*mostra)(void*) ) { //A função que exibe o dado é passada como parâmetro
	if(pilhaVazia1(pilha)){
		printf("\nPilha 1 vazia!\n");
	}else{
		int i;
		printf("\n\tDados da Pilha 1:\n");
		for(i = 0; i <= pilha.topo1; i++){
			printf("\tDados[%i]: ", i+1);
			mostra(pilha.dados[i]); //A biblioteca utiliza a função passada como parâmetro para imprimir o dado.
			printf("\n");
		}
		printf("\n");
	}
}

void mostraPilha2(Pilha pilha, void (*mostra)(void*) ) { //A função que exibe o dado é passada como parâmetro
	if(pilhaVazia2(pilha)){
		printf("\nPilha 2 vazia!\n");
	}else{
		int i;
		printf("\n\tDados da Pilha 2:\n");
		for(i = pilha.capacidade-1; i >= pilha.topo2; i--){
			printf("\tDados[%i]: ", i+1);
			mostra(pilha.dados[i]); //A biblioteca utiliza a função passada como parâmetro para imprimir o dado.
			printf("\n");
		}
		printf("\n");
	}
}

void desalocaPilha(Pilha* pilha){
	int i;

	for(i=0; i < pilha->topo1; i++){
		free(pilha->dados[i]);
	}

	for(i=pilha->capacidade; i > pilha->topo2; i--){
		free(pilha->dados[i]);
	}

	free(pilha->dados);
}

int pilhaVazia1(Pilha pilha) {
	if(pilha.topo1 == -1 ) {
		return 1;
	}else{
		return 0;
	}
}

int pilhaVazia2(Pilha pilha) {
	if(pilha.topo2 == pilha.capacidade) {
		return 1;
	}else{
		return 0;
	}
}

int pilhaCheia(Pilha pilha) {
	if(pilha.topo1 == pilha.topo2-1){
		return 1;
	}else {
		return 0;
	}
}