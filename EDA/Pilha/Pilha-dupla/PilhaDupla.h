#define ERRO_PILHA_CHEIA -1
#define ERRO_PILHA_VAZIA -2

typedef struct {
	void** dados;
	int topo1;
	int topo2;
	int capacidade;
	int tamInfo;
} Pilha;

void inicializaPilha(Pilha* pilha, int capacidade, int tamInfo);

int empilha1(Pilha* pilha, void* info);

int empilha2(Pilha* pilha, void* info);

int desempilha1(Pilha* pilha, void* destino);

int desempilha2(Pilha* pilha, void* destino);

void mostraPilha1(Pilha pilha, void (*mostra)(void*) ) ;

void mostraPilha2(Pilha pilha, void (*mostra)(void*) ) ;

void desalocaPilha(Pilha* pilha);

int pilhaVazia1(Pilha pilha) ;

int pilhaVazia2(Pilha pilha) ;

int pilhaCheia(Pilha pilha) ;
