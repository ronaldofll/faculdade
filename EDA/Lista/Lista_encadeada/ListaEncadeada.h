#define ERRO_LISTA_VAZIA -1;

typedef struct elem {
	void* info;
	struct elem* proximo;
}Elemento;

typedef struct{
	int tamInfo;
	Elemento* cabeca;//Elemento* cabeca;
}Lista;

void mostraLista(Lista l, void(*mostra)(void*));

void inicializaLista(Lista* l, int t);

int listaVazia(Lista l1);

int insereNoInicio(Lista* l, void* x);

int removeDoInicio(Lista* l, void* destino);

int insereNoFim(Lista* l, void* x);

int removeDoFim(Lista* l, void* destino);