#include "ListaEncadeada.h"
#include <stdio.h>

void mostra_float(void * x);

int main(){
	Lista l1;

	inicializaLista(&l1, sizeof(float));

	float x = 5;

	insereNoInicio(&l1, &x);

	x = 6;

	insereNoInicio(&l1,&x);

	mostraLista(l1, mostra_float);

	float y;
	removeDoInicio(&l1, &y);

	mostraLista(l1, mostra_float);
	printf("\nValor removido: %.2f\n", y);

	x = 8;
	insereNoFim(&l1, &x);

	mostraLista(l1, mostra_float);

}

void mostra_float(void* x) {
	printf("%.2f\n", *(float*)x);
}
