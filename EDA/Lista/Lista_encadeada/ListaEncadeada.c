#include "ListaEncadeada.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


void inicializaLista(Lista* l1, int t){
	l1->tamInfo = t;
	l1->cabeca = NULL;
}

void mostraLista(Lista l1, void(*mostra)(void*)){
	if(listaVazia(l1)){
		printf("Lista vazia\n");
	}
	else{
		printf("\nDados da lista\n");
		Elemento *p = l1.cabeca;
		int i;
		while(p!=NULL){
			mostra(p->info);
			p = p->proximo;
		}
	}

}

Elemento* alocaElemento(void* info, int tamInfo){
	Elemento * p = malloc(sizeof(Elemento));
	if(p == NULL){
		return NULL;
	}
	p->info = malloc(tamInfo);
	if(p->info == NULL){
		free(p);
		return NULL;
	}	
	memcpy(p->info, info, tamInfo);

	return p;
}

int listaVazia(Lista l1){
	return l1.cabeca == NULL;
}

int insereNoInicio(Lista* l1, void * x){
	Elemento* p = alocaElemento(x, l1->tamInfo);
	if(p == NULL) {
		return 0;
	}

	p->proximo = l1->cabeca;
	l1->cabeca = p;
	return 1;
}

int removeDoInicio(Lista* l, void* destino){
	if(listaVazia(*l)){
		return ERRO_LISTA_VAZIA;
	}

	Elemento* p = l->cabeca; 
	l->cabeca = p->proximo;  
	memcpy(destino, p->info, l->tamInfo);
	free(p->info);
	free(p);
	return 1;
}

int insereNoFim(Lista* l, void* x) {
	if(listaVazia(*l)){
		return insereNoInicio(l, x);
	}

	Elemento* novo = alocaElemento(x, l->tamInfo);
	if(novo == NULL){
		return 0;
	}

	Elemento* p = l->cabeca;
	while(p->proximo != NULL) {
		p = p->proximo;
	}
	p->proximo = novo;
	novo->proximo = NULL;

	return 1;
}

int removeDoFim(Lista* l, void* destino) {

}