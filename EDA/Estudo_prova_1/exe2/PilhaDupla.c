#include <stdio.h>
#include <stdlib.h>
#include "PilhaDupla.h"

#define ERRO_PILHA_CHEIA -1;
#define ERRO_PILHA_VAZIA -2;

void inicializa(PilhaDupla* p, int capacidade){
	p->dados = malloc(capacidade * sizeof(int));
	p->capacidade = capacidade;
	p->dados[0] = 0;
	p->dados[capacidade - 1] = capacidade -1;
}

int pilha1_vazia(PilhaDupla p){
	if(p.dados[0] == 0){
		return 1;
	}

	return 0;
}

int pilha2_vazia(PilhaDupla p){
	if(p.dados[p.capacidade-1] == p.capacidade-1){
		return 1;
	}

	return 0;
}

int pilhas_cheias(PilhaDupla p){
	if(p.dados[0] == (p.dados[p.capacidade-1] - 1) ) {
		return 1;
	}

	return 0;
}

int empilha1(PilhaDupla* p, int x){
	if(pilhas_cheias(*p)){
		return ERRO_PILHA_CHEIA;
	}

	int* topo = &p->dados[0];
	p->dados[++(*topo)] = x;
	return 1;
}

int empilha2(PilhaDupla* p, int x){
	if(pilhas_cheias(*p)){
		return ERRO_PILHA_CHEIA;
	}

	int* topo = &p->dados[p->capacidade - 1];
	p->dados[--(*topo)] = x;
	return 1;

}

int desempilha1(PilhaDupla* p, int* x){
	if(pilha1_vazia(*p)){
		return ERRO_PILHA_VAZIA;
	}

	int* topo = &p->dados[0];
	*x = p->dados[(*topo)--];
	return 1;
}

int desempilha2(PilhaDupla* p, int* x){
	if(pilha2_vazia(*p)){
		return ERRO_PILHA_VAZIA;
	}

	int* topo = &p->dados[p->capacidade - 1];
	*x = p->dados[(*topo)++];
	return 1;

}

void mostra(PilhaDupla p){
	int topo1 = p.dados[0];
	int topo2 = p.dados[p.capacidade -1];
	int i;

	printf("\n\n########################################\n");
	printf("\nPilhaDupla {\n");
	printf("\tCapacidade: %i\n", p.capacidade-2);
	printf("\tTopo 1 = %i\n", topo1);
	printf("\tTopo 2 = %i\n", topo2);
	printf("}\n");

	printf("\nPilha 1:\n");
	if(pilha1_vazia(p)){
		printf("Vazia!\n");
	}else{
		for(i = 0; i <= topo1; i++){
			printf("\t[%02i] = %i\n", i+1, p.dados[i+1]);
		}
	}

	printf("\nPilha 2:\n");
	if(pilha2_vazia(p)){
		printf("Vazia!\n");
	}else{
		for(i = p.capacidade - 1; i >= topo2; i--){
			printf("\t[%02i] = %i\n", i+1, p.dados[i-1]);
		}
	}

	printf("\n########################################\n\n");
}
