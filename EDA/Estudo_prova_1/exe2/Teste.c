#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "PilhaDupla.h"

int main(){

	PilhaDupla p;
	inicializa(&p, 10);

	empilha1(&p, 1);
	empilha1(&p, 2);
	empilha1(&p, 3);
	empilha1(&p, 4);

	empilha2(&p, 5);
	empilha2(&p, 6);
	empilha2(&p, 7);

	mostra(p);

	int y;

	desempilha1(&p, &y);
	printf("\nDesempilhado: %i\n", y);
	desempilha2(&p, &y);
	printf("\nDesempilhado: %i\n", y);

	mostra(p);

	return 1;
}