

typedef struct{
	int *dados;
	int capacidade;
} PilhaDupla;

void inicializa(PilhaDupla* p, int capacidade);

int pilha1_vazia(PilhaDupla p);

int pilha2_vazia(PilhaDupla p);

int pilhas_cheias(PilhaDupla p);

int empilha1(PilhaDupla* p, int x);

int empilha2(PilhaDupla* p, int x);

int desempilha1(PilhaDupla* p, int* x);

int desempilha2(PilhaDupla* p, int* x);

void mostra(PilhaDupla p); //Apenas para teste

