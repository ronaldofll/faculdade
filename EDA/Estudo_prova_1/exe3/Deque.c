#include "Deque.h"
#include <stdio.h>
#include <stdlib.h>

void inicializar(Deque* d, int capacidade) {
	d->dados = malloc(capacidade * sizeof(int));
	d->capacidade = capacidade;
	d->esquerda = 0;
	d->direita = capacidade-1;
	d->qtd = 0;
}

int inserir_direita(Deque* d, int valor) {
	if(deque_cheio(*d)){
		return ERRO_DEQUE_CHEIO;
	}

	if(d->direita == d->capacidade-1) {
		d->direita = 0;
	}else{
		d->direita++;
	}

	d->qtd++;

	d->dados[d->direita] = valor;

	return 1;

}

int inserir_esquerda(Deque* d, int valor) {

	if(deque_cheio(*d)){
		return ERRO_DEQUE_CHEIO;
	}

	if(d->esquerda == 0) {
		d->esquerda = d->capacidade-1;
	}else{
		d->esquerda--;
	}

	d->qtd++;

	d->dados[d->esquerda] = valor;

	return 1;

}

int remover_direita(Deque* d, int* destino) {
	if(deque_vazio(*d)){
		return ERRO_DEQUE_VAZIO;
	}

	*destino = d->dados[d->direita];

	d->qtd--;

	if(d->direita == 0) {
		d->direita = d->capacidade-1;
	}else{
		d->direita--;
	}

	return 1;
}

int remover_esquerda(Deque* d, int* destino) {
	if(deque_vazio(*d)){
		return ERRO_DEQUE_VAZIO;
	}

	*destino = d->dados[d->esquerda];

	d->qtd--;

	if(d->esquerda == d->capacidade-1) {
		d->esquerda = 0;
	}else{
		d->esquerda++;
	}

	return 1;
}

int deque_cheio(Deque d) {
	if(d.qtd == d.capacidade){
		return 1;
	}

	return 0;
}

int deque_vazio(Deque d) {
	if(d.qtd == 0){
		return 1;
	}

	return 0;

}

void mostra(Deque d) {
	if(deque_vazio(d)) {
		printf("\nDeque Vazio!\n");
		return;
	}

	int i = d.esquerda, count = 0;

	printf("\n#########################\n");
	do {
		count++;
		printf("[%02i]: %i\n", i, d.dados[i]);

		if(i == d.capacidade-1){
			i = 0;
		}else{
			i++;
		}
	} while(count < d.qtd);
	
	printf("\n#########################\n");
}	
