
typedef struct {
	void** dados;
	int topo;
	int capacidade;
	int tamInfo;
} PilhaGenerica;


void inicializaPilha(PilhaGenerica* pilha, int capacidade, int tamInfo); // inicializaPilha(&pilha, capacidade, sizeof(float))

int empilha(PilhaGenerica* pilha, void* valor);

int desempilha(PilhaGenerica* pilha, void* destino);

void mostraPilha(PilhaGenerica pilha, void (*mostra)(void*));

void desalocaPilha(PilhaGenerica* pilha);

int pilhaVazia(PilhaGenerica pilha);

int pilhaCheia(PilhaGenerica pilha);
