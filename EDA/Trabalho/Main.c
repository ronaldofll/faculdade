#include "CalculadoraMatriz.h"
#include <stdio.h>
#include <stdlib.h>

void mostrarMatriz(void* matriz);
int removeDaListaDeMatriz(Lista* listaMatriz, int pos);

void menu();
void menuCriarMatriz();
void menuMostrarMatriz(int mostrarTitulo);
void menuInserir(int mostrarTitulo);
void menuCalculadora(int mostrarTitulo);
void menuRemoverMatriz();

Lista listaMatriz;

int main(){

	int x;

	inicializaLista(&listaMatriz, sizeof(Matriz));
	
	Matriz matriz1;
	inicializaMatriz(&matriz1, 2, 2);

	Matriz matriz2;
	inicializaMatriz(&matriz2, 2, 2);

	x = 1;
	insereNaMatriz(&matriz1, 1, 1, &x);
	x = 2;
	insereNaMatriz(&matriz1, 1, 2, &x);
	x = 3;
	insereNaMatriz(&matriz1, 2, 1, &x);
	x = 4;
	insereNaMatriz(&matriz1, 2, 2, &x);

	x = 4;
	insereNaMatriz(&matriz2, 1, 1, &x);
	x = 3;
	insereNaMatriz(&matriz2, 1, 2, &x);
	x = 2;
	insereNaMatriz(&matriz2, 2, 1, &x);
	x = 1;
	insereNaMatriz(&matriz2, 2, 2, &x);

	insereNoFim(&listaMatriz,&matriz1);
	insereNoFim(&listaMatriz,&matriz2);

	menu();
	
	return 1;
}

void menuCriarMatriz() {
	Matriz matriz1;
	int linha, coluna;
	
	printf("--------------------\n");
	printf("*** CRIAR MATRIZ ***\n");
	printf("--------------------\n");

	printf("Defina a quantidade de linhas e colunas\n");
	printf("Qtde de linhas: ");
	scanf("%d", &linha);
	printf("Qtde de colunas: ");
	scanf("%d", &coluna);

	inicializaMatriz(&matriz1, linha, coluna); //Inicializa matriz com a qtde de linhas e colunas
												// definidas pelo usuário

	char continuar;
	printf("Sua matriz foi criada com sucesso!\nDeseja inserir valores? S[im] ou N[ao]: ");
	getchar();
	scanf ("%c", &continuar);

	while ((continuar == 'S') || (continuar == 's')) // Se optar por Sim, deve informar a linha, coluna
	{												 // e valor para inserir na Matriz criada
		int valor;
		printf("Linha: ");
		scanf("%d", &linha);
		printf("Coluna: ");
		scanf("%d", &coluna);
		printf("Valor: ");
		scanf("%d", &valor);

		insereNaMatriz(&matriz1, linha, coluna, &valor);

		printf("Deseja continuar? S[im] ou N[ao]: ");
		getchar();
		scanf ("%c", &continuar);
	}

	insereNoFim(&listaMatriz, &matriz1); //Inserir a nova matriz no fim da lista de matrizes
	menu();
}

void menuInserir(int mostrarTitulo) {

	if(quantidadeElemento(listaMatriz) < 1) {
		printf("\nÉ necessário pelo menos 1 matriz para esta operação.\n");
		menu();
		return;
	}
	
	if(mostrarTitulo){
		printf("---------------------------------\n");
		printf("*** INSERIR VALORES NA MATRIZ ***\n");
		printf("---------------------------------\n");
	}

	int posicao;
	printf("*** Escolha a matriz\n");
	mostraLista(listaMatriz, mostrarMatriz);
	scanf("%d", &posicao);					 // para as Matrizes criadas

	Matriz* endMatriz;
	endMatriz = retornaInfoNaPos(&listaMatriz, posicao-1); // funcao retorna o endereço da Matriz
	if (endMatriz == NULL){	// Se for nula, nao foi localizada
		printf("Posicao invalida!\n");
		menuInserir(0);
		return;
	}

	int linha, coluna, valor;
	printf("Linha: ");
	scanf("%d", &linha);
	printf("Coluna: ");
	scanf("%d", &coluna);
	printf("Valor: ");
	scanf("%d", &valor);

	insereNaMatriz(endMatriz, linha, coluna, &valor); // Insere o valor na coordenada informada

	char continuar;
	printf("Deseja continuar? S[im] ou N[ao] ");
	getchar();
	scanf ("%c", &continuar);

	if(continuar == 'S' || continuar == 's') {
		menuInserir(0);
		return;
	}
	menu();
}

void menuMostrarMatriz(int mostrarTitulo) {
	
	if(listaVazia(listaMatriz)) {
		printf("\nNenhuma matriz criada, crie uma matriz para exibi-lá.\n");
		menu();
		return;
	}

	if(mostrarTitulo) {
		printf("----------------------\n");
		printf("*** MOSTRAR MATRIZ ***\n");
		printf("----------------------\n");
	}

	int posicao;
	printf("*** Qual a posição da matriz? ");
	scanf("%d", &posicao);

	Matriz* endMatriz;
	endMatriz = retornaInfoNaPos(&listaMatriz, posicao-1);

	if (endMatriz == NULL) {
		printf("Posicao invalida!\n");
		menuMostrarMatriz(0);
		return;
	}

	mostraMatriz(*endMatriz); // Exibe os dados na tela
	menu();
}

void menuCalculadora(int mostrarTitulo) {

	if(quantidadeElemento(listaMatriz) < 1) {
		printf("\nÉ necessário pelo menos 1 matriz para esta operação.\n");
		menu();
		return;
	}

	Matriz matriz1, matriz2;
	Matriz* resultado;

	int operacao;

	if(mostrarTitulo) {
		printf("-----------------------------\n");
		printf("*** CALCULADORA DE MATRIZ ***\n");
		printf("-----------------------------\n");
	}
	printf("Digite a posicao das duas matrizes para o calculo: \n");

	int posicao;
	printf("*** Matriz 1: ");
	scanf("%d", &posicao);
	while(leNaPosicao(listaMatriz, &matriz1, posicao-1) != 1){
		printf("Posicao invalida!\n");
		printf("*** Matriz 1: ");
		scanf("%d", &posicao);
	}

	mostraMatriz(matriz1);

	printf("*** Matriz 2: ");
	scanf("%d", &posicao);
	while(leNaPosicao(listaMatriz, &matriz2, posicao-1) != 1){	
		printf("Posicao invalida!\n");
		printf("*** Matriz 2: ");
		scanf("%d", &posicao);
	}

	mostraMatriz(matriz2);

	do { // Loop para garantir que será executada uma opcao valida de operacao

		printf("Selecione uma operacao: \n");
		printf("1) Soma: \n");
		printf("2) Subtracao: \n");
		printf("3) Multiplicacao: \n");
		scanf("%d", &operacao);
		
		printf("---------------------\n");
		printf("RESULTADO DO CALCULO:\n");
		printf("---------------------\n");

		switch (operacao)
		{
			// As funcoes da calculadora retornam a nova matriz, resultado do calculo
			case 1:
				resultado = somaMatriz(matriz1, matriz2);
				break;

			case 2:
				resultado = subtracaoMatriz(matriz1, matriz2);
				break;

			case 3:
				resultado = multiplicacaoMatriz(matriz1, matriz2);
				break;

			default:
			{
				printf("opção invalida!\n");
				break;
			}
		}
	} while ((operacao != 1) && (operacao != 2) && (operacao != 3));
	
	if(resultado != NULL){
		mostraMatriz(*resultado);
		insereNoFim(&listaMatriz, resultado);
	}else {
		menuCalculadora(0);
		return;
	}

	menu();
}

void menuRemoverMatriz() {
	Matriz* matriz = NULL;

	printf("----------------------\n");
	printf("*** REMOVER MATRIZ ***\n");
	printf("----------------------\n");

	int posicao;
	do{
		printf("*** Escolha a matriz à remover\n");
		mostraLista(listaMatriz, mostrarMatriz);
		scanf("%d", &posicao);
	}while(!removeDaListaDeMatriz(&listaMatriz, posicao-1));

	printf("MATRIZ REMOVIDA COM SUCESSO!\n");
	menu();
}

void menu() {
	int opcao;

	printf("\n--------------\n");
	printf("#### MENU ####\n");
	printf("--------------\n");
	printf("1) Criar Matriz\n");
	printf("2) Inserir Dados na Matriz\n");
	printf("3) Mostrar Matriz\n");
	printf("4) Calculadora de matriz\n");
	printf("5) Remover Matriz\n");
	printf("6) Sair\n");
	scanf("%i", &opcao);

	switch (opcao) {
		case 1: // Criar Matriz
			menuCriarMatriz();
			break;
		case 2:
			menuInserir(1);
			break;
		case 3: 
			menuMostrarMatriz(1);
			break;
		case 4:
			menuCalculadora(1);
			break;
		case 5:
			menuRemoverMatriz();
			break;
		case 6:
			return;
		default:
			printf("\nOpção inválida!\n");
			menu();
	}
}

void mostrarMatriz(void* pMatriz) {
	Matriz matriz = *(Matriz*)pMatriz;
	mostraMatriz(matriz);
}

int removeDaListaDeMatriz(Lista* listaMatriz, int pos) {
	Matriz* matrizARemover = retornaInfoNaPos(listaMatriz, pos); //Primeiro busca a matriz para desalocar seus elementos depois de remover da lista

	if(matrizARemover == NULL) {
		printf("\nPosição inválida!");
		return 0;
	}
	
	Matriz matrizRemovida; //Apenas para utiizar na função removeDaPos
	desalocaMatriz(matrizARemover); //Desaloca a matriz para que a lista, elementos e entradas não fiquem na memoria;
	removeDaPos(listaMatriz, &matrizRemovida, pos); //Remove a matriz da lista
	return 1;
}
