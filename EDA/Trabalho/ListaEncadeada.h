#define ERRO_LISTA_VAZIA -1
#define ERRO_POS_INVALIDA -2

typedef struct elem {
	void* info;
	struct elem* proximo;
}Elemento;

typedef struct{
	int tamInfo;
	Elemento* cabeca;
}Lista;

void inicializaLista(Lista* l1, int t);

int insereNoInicio(Lista* l1, void * x);

void mostraLista(Lista l1, void(*mostra)(void*));

int removeDoInicio(Lista* l1, void* destino);

int insereNoFim(Lista* l1, void * x);

Elemento* alocaElemento(void* info, int t);

int listaVazia (Lista l);

int removeDoFim(Lista *l, void *info);

int desalocaLista(Lista *l);

int insereNaPos(Lista* l, void* info, int pos);

int removeDaPos(Lista* l, void* destino, int pos);

int leNaPosicao(Lista l, void *valor, int pos);

int quantidadeElemento(Lista l);

int busca(Lista l, void* info, int(*compara)(void*, void*));

int insereEmOrdem(Lista* l, void *valor, int (*compara)(void*, void*));

void* retornaInfoNaPos(Lista* l, int pos);