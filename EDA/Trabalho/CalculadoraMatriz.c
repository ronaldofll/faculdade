#include "CalculadoraMatriz.h"
#include "stdlib.h"
#include "stdio.h"

Matriz* somaMatriz(Matriz matriz1, Matriz matriz2)
{
    if((matriz1.linhas != matriz2.linhas) || (matriz1.colunas != matriz2.colunas))
    {
        printf("\nERRO: Incompatibilidade de tamanho\n\n");
        return NULL;
    }

    int i, j;
    Matriz* matriz3 = malloc(sizeof(Matriz));
    if(matriz3 == NULL) {
        printf("\nERRO: Nao foi possivel alocar memoria para a matriz resultante\n\n");
        return NULL;
    }
    inicializaMatriz(matriz3, matriz1.linhas, matriz1.colunas);

    for (i = 0; i < matriz1.linhas; i++)
    {
        for (j = 0; j < matriz1.colunas; j++)
        {
            int valor = leValorNaMatriz(matriz1, i, j) + leValorNaMatriz(matriz2, i, j);

            insereNaMatriz(matriz3, (i+1), (j+1), &valor);
        }
    }

    return matriz3;

}

Matriz* subtracaoMatriz(Matriz matriz1, Matriz matriz2)
{
    if((matriz1.linhas != matriz2.linhas) || (matriz1.colunas != matriz2.colunas))
    {
        printf("ERRO: Incompatibilidade de tamanho\n");
        return NULL;
    }
    int i, j;
    Matriz* matriz3 = malloc(sizeof(Matriz));
    if(matriz3 == NULL) {
        printf("\nERRO: Nao foi possivel alocar memoria para a matriz resultante\n\n");
        return NULL;
    }
    inicializaMatriz(matriz3, matriz1.linhas, matriz1.colunas);

    for (i = 0; i < matriz1.linhas; i++)
    {
        for (j = 0; j < matriz1.colunas; j++)
        {
            int valor = leValorNaMatriz(matriz1, i, j) - leValorNaMatriz(matriz2, i, j);

            insereNaMatriz(matriz3, (i+1), (j+1), &valor);
        }
    }

    return matriz3;
}

Matriz* multiplicacaoMatriz(Matriz matriz1, Matriz matriz2)
{
    if((matriz1.colunas != matriz2.linhas))
    {
        printf("\nERRO: Incompatibilidade de tamanho\n\n");
        return NULL;
    }

    int i, j, k;
    Matriz* matriz3 = malloc(sizeof(Matriz));
    if(matriz3 == NULL) {
        printf("\nERRO: Nao foi possivel alocar memoria para a matriz resultante\n\n");
        return NULL;
    }
    inicializaMatriz(matriz3, matriz1.linhas, matriz2.colunas);

    for (i = 0; i < matriz1.linhas; i++)
    {
        for (j = 0; j < matriz2.colunas; j++)
        {
            int valor = 0;
            for (k = 0; k < matriz2.linhas; k++)
            {
                valor = valor + (leValorNaMatriz(matriz1, i, k) * leValorNaMatriz(matriz2, k, j));
            }
            insereNaMatriz(matriz3, (i+1), (j+1), &valor);
        }

    }
    return matriz3;
}
