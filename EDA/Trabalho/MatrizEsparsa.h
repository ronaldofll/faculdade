#include "ListaEncadeada.h"

#define ERRO_ALOCACAO -20;
#define MATRIZ_ERRO_POS_INVALIDA -21;

typedef struct {
	Lista* lista;
	int linhas;
	int colunas;
} Matriz;

typedef struct {
	int indice;
	int* valor;
} Entrada;


int inicializaMatriz(Matriz* matriz, int linhas, int colunas);

int insereNaMatriz(Matriz* matriz, int linha, int coluna, int* info);

void mostraMatriz(Matriz matriz);

void desalocaMatriz(Matriz* matriz);

int leValorNaMatriz(Matriz matriz, int linha, int coluna);
