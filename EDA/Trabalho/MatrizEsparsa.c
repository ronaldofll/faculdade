#include "MatrizEsparsa.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int inicializaMatriz(Matriz* matriz, int linhas, int colunas) {
	Lista* lista = malloc(sizeof(Lista));
	if(lista == NULL) {
		printf("\nERRO: Não foi possível alocar memória para a lista da matriz\n\n");
		return ERRO_ALOCACAO;
	}
	inicializaLista(lista, sizeof(Lista));

	int i;
	for(i = 0; i < linhas; i++) {
		Lista* linha = malloc(sizeof(Lista));  //Esta lista representa a linha e será nela que os dados irão ser armazenados
		if(linha == NULL) {
			printf("\nERRO: Não foi possível alocar memória para a linha %d da matriz\n\n", i);
			return ERRO_ALOCACAO;
		}
		inicializaLista(linha, sizeof(Entrada));
		insereNoFim(lista, linha); //Já insere na inicialização, as listas que irão representar as linhas.
	}

	matriz->lista = lista;
	matriz->linhas = linhas;
	matriz->colunas = colunas;
	return 1;
}

Entrada* criaEntrada(int* valor, int indice) {
	Entrada* entrada = malloc(sizeof(Entrada));
	if(entrada == NULL) {
		printf("ERRO: Não foi possível alocar memória para a entrada da matriz\n\n");
		return NULL;
	}

	//Copia o valor que será inserido na matriz.
	int* valorEntrada = malloc(sizeof(int));
	if(valorEntrada == NULL) {
		printf("ERRO:  Não foi possível alocar memória para o valor de entrada\n\n");
	}
	memcpy(valorEntrada, valor, sizeof(int));

	entrada->indice = indice;
	entrada->valor = valorEntrada;
	return entrada;
}

int comparaIndiceEntrada(void* a, void* b) {
	Entrada x = *(Entrada*)a;
	Entrada y = *(Entrada*)b;
	return (x.indice < y.indice) ? -1 : (x.indice > y.indice) ? 1 : 0;
}

void redimensionaMatriz(Matriz* matriz, int linhas, int colunas) {
	if(colunas > matriz->colunas) {
		matriz->colunas = colunas;
	}

	if(linhas > matriz->linhas) {

		int i;
		for(i = matriz->linhas; i < linhas; i++) {
			Lista linha;
			inicializaLista(&linha, sizeof(Entrada));
			insereNoFim(matriz->lista, &linha);
		}
		matriz->linhas = linhas;
	}
}

int insereNaMatriz(Matriz* matriz, int linha, int coluna, int* info) {
	if(linha > matriz->linhas || coluna > matriz->colunas) {
		redimensionaMatriz(matriz, linha, coluna);
	}

	//Primeira lista, equivale as linhas da matriz.
	Lista* listaLinhas = matriz->lista;

	//Encontra a linha onde será adicionado o valor.
	Elemento* elemLinha = listaLinhas->cabeca;
	int cont = 0;
	while(cont < linha-1) {
		elemLinha = elemLinha->proximo;
		cont++;
	}

	//Lista de elementos, cada elemento possui um ponteiro para uma Entrada, onde será armazenado o valor.
	Lista* listaEntradas = elemLinha->info;

	Entrada* entrada = criaEntrada(info, coluna-1);

	int posicaoAntigaEntrada = busca(*listaEntradas, entrada, comparaIndiceEntrada);

	if(posicaoAntigaEntrada > -1) {
		Entrada temp;
		removeDaPos(listaEntradas, &temp, posicaoAntigaEntrada);
	}

	//Insere na lista a Entrada
	if(info != NULL && *info != 0){
		insereEmOrdem(listaEntradas, entrada, comparaIndiceEntrada);
	}

	return 1;

}

void mostraMatriz(Matriz matriz) {

	int i = 0, j = 0;

	Lista* linhas = matriz.lista;
	Elemento* elemLinha = linhas->cabeca;
	while(i < matriz.linhas) {

		Lista* linha = NULL;
		if(elemLinha != NULL && elemLinha->info != NULL){
			linha = elemLinha->info;
		}

		Elemento* elemXY = NULL;
		if(linha != NULL && linha->cabeca != NULL) {
			elemXY = linha->cabeca;
		}

		j = 0;
		while(j < matriz.colunas){
			Entrada* entrada = NULL;
			if(elemXY != NULL && elemXY->info != NULL) {
				entrada = elemXY->info;
				if(entrada != NULL && entrada->indice == j){
					printf("%4d ", *(entrada->valor));
					elemXY = elemXY -> proximo;
				}else{
					printf("%4d ", 0);
				}
			}else{
				printf("%4d ", 0);
			}
			j++;
		}

		printf("\n\n");
		elemLinha = elemLinha->proximo;
		i++;
	}
}

int leValorNaMatriz(Matriz matriz, int linha, int coluna) {
	if(linha > matriz.linhas) {
		return MATRIZ_ERRO_POS_INVALIDA;
	}

	if(coluna > matriz.colunas) {
		return MATRIZ_ERRO_POS_INVALIDA;
	}

	Lista* listaLinhas = matriz.lista;

	Elemento* elemLinha = listaLinhas->cabeca;
	int cont = 0;
	while(cont < linha) {
		elemLinha = elemLinha->proximo;
		cont++;
	}

	Lista* listaEntradas = elemLinha->info;
	Elemento* elemCol = listaEntradas->cabeca;

	while(elemCol != NULL) {
		Entrada* entrada = elemCol->info;
		if(entrada->indice == coluna) {
			return *(entrada->valor);
		}
		elemCol = elemCol->proximo;
	}

	return 0;
}

void desalocaMatriz(Matriz* matriz) {

	Lista* linhas; //Lista contendo elementos que contém listas
	Elemento* elemLinha; //Elemento contendo lista
	//Lista* linha; //Lista que representa a linha da matriz
	//Elemento* elemEntrada; //Elemento contendo Entrada
	//Entrada* entrada; //Entrada com índice e valor na coordenada xy

	int i = 0, j = 0;

	linhas = matriz->lista;
	if(linhas == NULL) {
		return;
	}

	elemLinha = linhas->cabeca;
	while(i < matriz->linhas) { //Percorre todas as linhas da matriz
		Lista* linha = NULL;
		if(elemLinha != NULL && elemLinha->info != NULL){
			linha = elemLinha->info;
		}

		Elemento* elemEntrada = NULL;
		if(linha != NULL && linha->cabeca != NULL) {
			elemEntrada = linha->cabeca;
		}

		j = 0;
		while(j < matriz->colunas){ //Percorre todos os elementos da linha da matriz
			Entrada* entrada = NULL;
			if(elemEntrada != NULL && elemEntrada->info != NULL) {
				entrada = elemEntrada->info;
				if(entrada != NULL){
					Elemento* aux = elemEntrada;
					elemEntrada = elemEntrada -> proximo;

					free(entrada->valor);
					free(entrada);
					free(aux);
				}
			}
			j++;
		}

		elemLinha = elemLinha->proximo;
		free(linha);
		i++;
	}

	free(linhas->cabeca);
}
