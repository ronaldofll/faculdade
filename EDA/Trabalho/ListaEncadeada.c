#include "ListaEncadeada.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void inicializaLista(Lista* l1, int t){
	l1->tamInfo = t;
	l1->cabeca = NULL;
}

int insereNoInicio(Lista* l1, void * x){
	Elemento* p = malloc(sizeof(Elemento));
	p->info = malloc(sizeof(l1->tamInfo));
	memcpy(p->info, x, l1->tamInfo);
	p->proximo = l1->cabeca;
	l1->cabeca = p;

	return 1;
}

void mostraLista(Lista l1, void(*mostra)(void*)){
	if(listaVazia(l1)){
		printf("Lista vazia\n");
	}
	else{
		Elemento *p = l1.cabeca;
		int i = 1;
		while(p!=NULL){
			printf("\n*** Matriz %d ***\n", i);
			mostra(p->info);
			p = p->proximo;
			i++;
		}
	}

}

int listaVazia(Lista l1){
	return l1.cabeca == NULL;
}

int removeDoInicio(Lista* l1, void* destino){
	if(listaVazia(*l1)){
		return  ERRO_LISTA_VAZIA;
	}

	Elemento* p = l1->cabeca;
	memcpy(destino, p->info, l1->tamInfo);

	l1->cabeca = p->proximo;

	free(p->info);
	free(p);

	return 1;
}

int insereNoFim(Lista *l, void *info)
{
    if(listaVazia(*l))
        return insereNoInicio(l, info);

    Elemento *novo = alocaElemento(info, l->tamInfo);

    if (novo == NULL)
        return 0;

    Elemento *p = l->cabeca;

    while(p->proximo != NULL)
    {
        p = p->proximo;
    }

    p->proximo = novo;

    novo->proximo = NULL;

    return 1;
}

Elemento* alocaElemento(void* info, int t){
	Elemento* p = malloc(sizeof(Elemento));
	if(p == NULL){
		return NULL;
	}
	p->info = malloc(t);
	if(p->info == NULL){
		free(p);
		return NULL;
	}
	memcpy(p->info, info, t);

	return p;
}

int removeDoFim(Lista *l, void *info)
{
    if(listaVazia(*l))
        return ERRO_LISTA_VAZIA;

    if(l->cabeca->proximo == NULL)
        removeDoInicio(l, info);

    Elemento *p = l->cabeca;

    while(p->proximo->proximo != NULL)
    {
        p = p->proximo;
    }

    memcpy(info, p->proximo->info, l->tamInfo);

    free(p->proximo->info);
    free(p->proximo);

    p->proximo = NULL;

    return 1;
}


int desalocaLista(Lista *l)
{
    if(!listaVazia(*l))
    {
        Elemento *p = l->cabeca;
        Elemento *pAux;

        while(p->proximo != NULL)
        {
            pAux = p->proximo;
            free(p->info);
            free(p);
            p = pAux;
        }

        free(p->info);
        free(p);
        l->cabeca = NULL;

        return 1;
    }

    return 0;
}

int insereNaPos(Lista* l, void* info, int pos) {
	if(pos < 0) {
		return ERRO_POS_INVALIDA;
	}

	if(pos == 0 ) {
		return insereNoInicio(l, info);
	}

	if(listaVazia(*l)){
		return ERRO_POS_INVALIDA;
	}

	Elemento *p = l->cabeca;
	int count = 0;

	while(count < pos-1 && p->proximo != NULL) {
		p = p->proximo;
		count++;
	}

	if(count != pos-1) {
		return ERRO_POS_INVALIDA;
	}

	Elemento* novo = alocaElemento(info, l->tamInfo);
	if(novo == NULL) {
		return 0;
	}

	novo->proximo = p->proximo;
	p->proximo = novo;

	return 1;

}

int removeDaPos(Lista* l, void* destino, int pos) {

	if(listaVazia(*l)){
		return ERRO_LISTA_VAZIA;
	}

	if(pos < 0) {
		return ERRO_POS_INVALIDA;
	}

	if(pos == 0 ) {
		return removeDoInicio(l, destino);
	}

	Elemento *p = l->cabeca;
	int count = 0;

	while(count < pos-1 && p->proximo != NULL) {
		p = p->proximo;
		count++;
	}

	if(p->proximo == NULL) {
		return ERRO_POS_INVALIDA;
	}

	Elemento *aux = p->proximo;
	p->proximo = aux->proximo;

	memcpy(destino, aux->info, l->tamInfo);
	free(aux->info);
	free(aux);

	return 1;

}

int leNaPosicao(Lista l, void *valor, int pos) {
	if(pos < 0) {
		return ERRO_POS_INVALIDA;
	}

	if(listaVazia(l)){
		return ERRO_LISTA_VAZIA;
	}

	Elemento* p = l.cabeca;
	int cont = 0;

	while(cont < pos && p->proximo != NULL) {
		p = p->proximo;
		cont++;
	}

	if(cont != pos) {
		return ERRO_POS_INVALIDA;
	}

	memcpy(valor, p->info, l.tamInfo);

	return 1;
}

int quantidadeElemento(Lista l) {
	if(listaVazia(l)) {
		return 0;
	}

	int cont = 0;
	Elemento* p = l.cabeca;

	while(p != NULL) {
		p = p->proximo;
		cont++;
	}

	return cont;
}

int busca(Lista l, void* valor, int(*compara)(void*, void*)) {
	Elemento* p = l.cabeca;

	int cont = 0;
	while(p != NULL && compara(valor, p->info) != 0) {
		p = p->proximo;
		cont++;
	}

	if(p == NULL) {
		return -1;
	}

	return cont;
}

int insereEmOrdem(Lista* l, void *valor, int (*compara)(void*, void*)) {
	Elemento* p = l->cabeca;
	int cont = 0;

	while(p != NULL && compara(valor, p->info) > 0 ) {
		p = p-> proximo;
		cont++;
	}

	return insereNaPos(l, valor, cont);
}

void* retornaInfoNaPos(Lista* l, int pos) {
	if(listaVazia(*l)){
		return NULL;
	}

	if(pos < 0) {
		return NULL;
	}

	Elemento *p = l->cabeca;

	int count = 0;
	while(count < pos && p->proximo != NULL) {
		p = p->proximo;
		count++;
	}

	if(count < pos) {
		return NULL;
	}

	return p->info;
}
