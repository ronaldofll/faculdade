
#define ERRO_LISTA_VAZIA -1
#define ERRO_POS_INVALIDA -2

typedef struct elemDuplo {
	void* Info;
	struct elemDuplo* ant;
	struct elemDuplo* suc;
}ElementoDuplo;

typedef struct{
	int tamInfo;
	struct elemDuplo* cabeca;//Elemento* cabeca;
}ListaDupla;

void inicializa_listaDupla(ListaDupla* l1, int t);

int insereNoInicio(ListaDupla* l1, void * x);

void mostra_listaDupla(ListaDupla l1, void(*mostra)(void*));

int removeDoInicio(ListaDupla* l1, void* destino);

int insereNoFim(ListaDupla* l1, void * x);

ElementoDuplo* aloca_elemento(void* Info, int t);

int listaDupla_vazia (ListaDupla l);

int removeDoFim(ListaDupla *l, void *info);

int desaloca_listaDupla(ListaDupla *l);

int insereNaPos(ListaDupla* l, void* info, int pos);

int leNaPosicao(ListaDupla l, void *valor, int pos);

int quantidadeElementoDuplo(ListaDupla l);

int busca(ListaDupla l, void* info, int(*compara)(void*, void*));