#include "listaDuplamenteEncadeada.h"
#include <stdio.h>

void mostra_float(void*x);
int compara_float(void* x, void* y);

int main(){
	ListaDupla l1;

    float x=1;
	inicializa_listaDupla(&l1, sizeof(float));
	//mostra_listaDupla(l1, mostra_float);
    //insereNoInicio(&l1, &x);
   // x=2;
    insereNoInicio(&l1, &x);
    float destino;
    //removeDoInicio(&l1, &destino);   
    //insereNoFim(&l1, &x);
    x=5;
    insereNaPos(&l1,&x,1);
    x=2;
    insereNoFim(&l1, &x);
    //removeDoFim(&l1, &x);
    x=5;
    insereNaPos(&l1,&x,0);
    mostra_listaDupla(l1, mostra_float);
    float valor;
    leNaPosicao(l1, &valor, 0);
    removeDaPos(&l1,&destino, 1);

    //mostra_listaDupla_invertido(l1, mostra_float);
    printf("Le na posição: %.1f\n", valor);
    //printf("\nBusca: %i\n", busca(l1, &x, compara_float));
    desaloca_listaDupla(&l1);
}

void mostra_float(void* x){
	printf("%.2f\n", *(float*)x);
}

int compara_float(void* x, void* y) {
    float* a = x;
    float* b = y;
    
    if(*a < *b) {
        return -1;
    }else if( *a > *b) {
        return 1;
    }else {
        return 0;
    }
}