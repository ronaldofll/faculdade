#include "listaDuplamenteEncadeada.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h> 


void inicializa_listaDupla(ListaDupla* l1, int t){
	l1->tamInfo = t;
	l1->cabeca = NULL;
}

int insereNoInicio(ListaDupla* l1, void * valor){
	ElementoDuplo* p = aloca_elemento(valor, l1->tamInfo);
	if(p==NULL){
		return 0;
	}
	p->suc = l1->cabeca;
	l1->cabeca = p;
	if(p->suc != NULL)
		p->suc->ant = p;
	return 1;
}

ElementoDuplo* aloca_elemento(void* valor, int tamInfo) {
	ElementoDuplo* p = malloc(sizeof(ElementoDuplo));
	if(p == NULL){
		return NULL;
	}
	p->Info = malloc(tamInfo);
	if(p->Info == NULL){
		free(p);
		return NULL;
	}
	memcpy(p->Info, valor, tamInfo);

	return p;
}

void mostra_listaDupla(ListaDupla l1, void(*mostra)(void*)){
	if(listaDupla_vazia(l1)){
		printf("Lista Dupla vazia\n");
	}
	else{
		printf("Dados da Lista Dupla\n");
		ElementoDuplo *p = l1.cabeca;

		while(p!=NULL){
			mostra(p->Info);
			p = p->suc;
		}
	}

}

void mostra_listaDupla_invertido(ListaDupla l1, void(*mostra)(void*)){
	if(listaDupla_vazia(l1)){
		printf("Lista Dupla vazia\n");
	}
	else{
		printf("Dados da Lista Dupla\n");
		ElementoDuplo *p = l1.cabeca;

		while(p->suc != NULL){
			p = p->suc;
		}
		while(p!=NULL){
			mostra(p->Info);
			p = p->ant;
		}
	}

}

int listaDupla_vazia(ListaDupla l1){
	return l1.cabeca == NULL;
}

int removeDoInicio(ListaDupla* l1, void* destino){
	if(listaDupla_vazia(*l1)){
		return  ERRO_LISTA_VAZIA;
	}

	ElementoDuplo* p = l1->cabeca;
	memcpy(destino, p->Info, l1->tamInfo);

	free(p->Info);
	free(p);
	p = l1->cabeca;
	if(p != NULL){
		p->ant = NULL;
	}

	return 1;
}

int insereNoFim(ListaDupla *l, void *valor) // O *l recebe o endereço de memória do l1 da main
{
    if(listaDupla_vazia(*l))
        return insereNoInicio(l, valor); //Passo o endereço de memória do l1 que está na main. Se eu passar o &l, estaria passando o endereço de memória do l do InsereNofim

    ElementoDuplo *novo = aloca_elemento(valor, l->tamInfo);

    if (novo == NULL)
        return 0; //deu erro na alocacao dentro de aloca_elemento e retorno 0, erro na operação

    ElementoDuplo *p = l->cabeca;

    while(p->suc != NULL)
    {
        p = p->suc; //p aponta para o endereço de memória do próximo elemento até chegar no último, que tem o próximo null
    }

    p->suc = novo; // Qdo sair do while, o p->proximo armazena o endereço do novo elemento

    novo->suc = NULL; // O próximo do último elemento, deve ser NULL

    novo->ant = p;

    return 1;
}

int removeDoFim(ListaDupla *l, void *info)
{
    if(listaDupla_vazia(*l))
        return ERRO_LISTA_VAZIA;

    if(l->cabeca->suc == NULL) // Se a lista tiver somente um elemento, ou seja, ele é começo e fim, chamo a RemoveDoInicio
        removeDoInicio(l, info);

    ElementoDuplo *p = l->cabeca; // Estou definindo que o meu p está apontando para o endereço da cabeça, que contém os dados do primeiro elemento

    while(p->suc->suc != NULL) // Enquanto o penultimo não for nulo
    {
        p = p->suc; //p aponta para o endereço de memória do próximo elemento até chegar no último, que tem o próximo null
    }
//primeiro informação, depois elemento no memcpy
    memcpy(info, p->Info, l->tamInfo); // Copiar, diretamente, o dado do ultimo elemento para a variável y da main

    free(p->Info); // Desalocar o espaço de memória alocado para a informação do ultimo elemento
    free(p->suc); // Estou passando para o free, o endereço de memória alocado para o ultimo elemento. Logo, estou desalocando o ultimo elemento

    p->suc = NULL;

    return 1;
}


int desaloca_listaDupla(ListaDupla *l)
{
    if(!listaDupla_vazia(*l))
    {
        ElementoDuplo *p = l->cabeca;
        ElementoDuplo *pAux;

        while(p->suc != NULL)
        {
            pAux = p->suc;
            free(p->Info);
            free(p);
            p = pAux;
        }

        free(p->Info);
        free(p);
        l->cabeca = NULL;

        return 1;
    }

    return 0;
}


int insereNaPos(ListaDupla* l, void* info, int pos) {
	if(pos < 0) {                        //POSIÇÃO NÃO PODE SER MENOR QUE 0.
		return ERRO_POS_INVALIDA;
	}
	if(pos == 0 ) {                      //SE POSIÇÃO FOR 0, ENTÃO CHAMA FUNÇÃO PARA INSERIR NO INICIO
		return insereNoInicio(l, info);
	}
	if(listaDupla_vazia(*l)){				//POSIÇÃO É MAIOR QUE 0 E A LISTA ESTÁ VAZIA, PORTANTO POSIÇÃO É INVÁLIDA.
		return ERRO_POS_INVALIDA;
	}
	ElementoDuplo *p = l->cabeca;
	int count = 0;

	while(count < pos-1 && p->suc != NULL) { //PERCORRE A LISTA ATÉ A POSIÇÃO INFORMADA OU ATÉ QUE A LISTA ACABE.
		p = p->suc;
		count++;
	}

	if(count != pos-1) {						//VERIFICA SE POSIÇÃO EXISTE NA LISTA
		return ERRO_POS_INVALIDA;
	}

	ElementoDuplo* novo = aloca_elemento(info, l->tamInfo);
	if(novo == NULL) {
		return 0;
	}

	novo->suc = p->suc;
	p->suc = novo;
	
	if(novo->suc != NULL){
		novo->suc->ant = novo;	
	}	
	novo->ant = p;

	return 1;

}


int removeDaPos(ListaDupla* l, void* destino, int pos) {	

	if(listaDupla_vazia(*l)){				//POSIÇÃO É MAIOR QUE 0 E A LISTA ESTÁ VAZIA, PORTANTO POSIÇÃO É INVÁLIDA.
		return ERRO_LISTA_VAZIA;
	}

	if(pos < 0) {                        //POSIÇÃO NÃO PODE SER MENOR QUE 0.
		return ERRO_POS_INVALIDA;
	}

	if(pos == 0 ) {                      //SE POSIÇÃO FOR 0, ENTÃO CHAMA FUNÇÃO PARA INSERIR NO INICIO
		return removeDoInicio(l, destino);
	}

	ElementoDuplo *p = l->cabeca;
	int count = 0;

	while(count < pos-1 && p->suc != NULL) { //PERCORRE A LISTA ATÉ A POSIÇÃO INFORMADA OU ATÉ QUE A LISTA ACABE.
		p = p->suc;
		count++;
	}

	if(p->suc == NULL) {						//VERIFICA SE POSIÇÃO A SER REMOVIDA EXISTE NA LISTA
		return ERRO_POS_INVALIDA;
	}

	ElementoDuplo *aux = p->suc;
	p->suc = aux->suc;

	if(p->suc != NULL){
		p->suc->ant = p;
	}

	memcpy(destino, aux->Info, l->tamInfo);
	free(aux->Info);
	free(aux);

	return 1;

}

int leNaPosicao(ListaDupla l, void *valor, int pos) {
	if(pos < 0) {
		return ERRO_POS_INVALIDA;
	}

	if(listaDupla_vazia(l)){
		return ERRO_LISTA_VAZIA;
	}

	ElementoDuplo* p = l.cabeca;
	int cont = 0;

	while(cont < pos && p->suc != NULL) {
		p = p->suc;
		cont++;
	}

	if(cont != pos) {
		return ERRO_POS_INVALIDA;
	}

	memcpy(valor, p->Info, l.tamInfo);

	return 1;
}

int quantidadeElemento(ListaDupla l) {
	if(listaDupla_vazia(l)) {
		return 0;
	}

	int cont = 0;
	ElementoDuplo* p = l.cabeca;

	while(p != NULL) {
		p = p->suc;
		cont++;
	}

	return cont;
}

int busca(ListaDupla l, void* valor, int(*compara)(void*, void*)) {
	ElementoDuplo* p = l.cabeca;

	int cont = 1;
	while(p != NULL && compara(valor, p->Info) != 0) {
		p = p->suc;
		cont++;
	}	

	if(p == NULL) {
		return -1;
	}

	return cont;
}