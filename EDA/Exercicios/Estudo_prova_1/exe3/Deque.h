
#define ERRO_DEQUE_CHEIO -1;
#define ERRO_DEQUE_VAZIO -2;

typedef struct {
	int* dados;
	int esquerda;
	int direita;
	int capacidade;
	int qtd;
} Deque;

void inicializar(Deque* d, int capacidade);

int inserir_direita(Deque* d, int valor);

int inserir_esquerda(Deque* d, int valor);

int remover_direita(Deque* d, int* destino);

int remover_esquerda(Deque* d, int* destino);

int deque_cheio(Deque d);

int deque_vazio(Deque d);

void mostra(Deque d);
