#include "Deque.h"
#include <stdio.h>
#include <stdlib.h>

int main() {

	Deque d;
	inicializar(&d, 5);

	inserir_esquerda(&d, 2);
	inserir_direita(&d, 3);

	inserir_direita(&d, 4);
	inserir_direita(&d, 5);

	inserir_esquerda(&d, 1);

	mostra(d);

	int y;
	remover_esquerda(&d, &y);
	printf("\nValor removido: %i\n", y);

	mostra(d);

	return 1;
}