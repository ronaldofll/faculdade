#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "PilhaGenerica.h"

int verifica_parenteses(char expressao[]);

int main(){

	int x = verifica_parenteses("Teste)");

	printf("\n%i\n", x);

	return 0;
}

int verifica_parenteses(char expressao[]){

	int n = strlen(expressao);

	PilhaGenerica pilha;
	inicializaPilha(&pilha, n, sizeof(char));

	char y;
	int i;
	for(i = 0; i < n; i++) {
		if(expressao[i] == '(') {
			empilha(&pilha, &expressao[i]);
		}else if(expressao[i] == ')') {
			if(!pilhaVazia(pilha)){
				desempilha(&pilha, &y);
			}else{
				desalocaPilha(&pilha);
				return 0;
			}
		}
	}
	
	desalocaPilha(&pilha);
	return pilhaVazia(pilha);
}