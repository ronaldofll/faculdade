#include "listaDuplamenteEncadeada.h"
#include <stdio.h>

void mostra_float(void*x);
int compara_float(void* x, void* y);

int main(){
	ListaDupla l1;
    inicializa_listaDupla(&l1, sizeof(float));

    float x=1;
    insereNoInicio(&l1, &x);
    x=2;
    insereNoInicio(&l1, &x);
    float destino;
    //removeDoInicio(&l1, &destino);   
    
    x = 3;
    insereNoFim(&l1, &x);
    x = 4;
    insereNoFim(&l1, &x);
    //removeDoFim(&l1, &x);
    mostra_listaDupla(l1, mostra_float);
    //mostra_listaDupla_invertido(l1, mostra_float);

    apaga_ultimos_elementos(&l1, 2);

    mostra_listaDupla(l1, mostra_float);

    desaloca_listaDupla(&l1);
}

void mostra_float(void* x){
	printf("%.2f\n", *(float*)x);
}

int compara_float(void* x, void* y) {
    float* a = x;
    float* b = y;
    
    if(*a < *b) {
        return -1;
    }else if( *a > *b) {
        return 1;
    }else {
        return 0;
    }
}